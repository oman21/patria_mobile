import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { AuthStatePage } from '../pages/auth-state/auth-state';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServiceProvider } from '../providers/service/service';
import { Network } from '@ionic-native/network';

import { HttpModule, Http } from '@angular/http';
import { DatabaseProvider } from '../providers/database/database';

import { IonicStorageModule } from '@ionic/storage';

import { Push, PushObject, PushOptions } from '@ionic-native/push';

import { Device } from '@ionic-native/device';
import { AppVersion } from '@ionic-native/app-version';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';

import { InAppBrowser } from '@ionic-native/in-app-browser';

import { DocumentViewer } from '@ionic-native/document-viewer';

import { Crop } from '@ionic-native/crop';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { SignaturePadModule } from 'angular2-signaturepad';

@NgModule({
  declarations: [
    MyApp,
    AuthStatePage,
    HomePage,
    LoginPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicImageViewerModule,
    SignaturePadModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AuthStatePage,
    HomePage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceProvider,
    Network,
    DatabaseProvider,
    Push,
    Device,
    AppVersion,
    FileTransfer,
    FileTransferObject,
    File,
    Camera,
    InAppBrowser,
    DocumentViewer,
    Crop
  ]
})
export class AppModule {}

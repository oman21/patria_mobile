import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Nav, Platform, AlertController, ToastController, MenuController  } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AuthStatePage } from '../pages/auth-state/auth-state';
import { HomePage } from '../pages/home/home';
import { Storage } from '@ionic/storage';
import { Push, PushObject, PushOptions} from '@ionic-native/push';

import { LoginPage } from '../pages/login/login';

import { AppVersion } from '@ionic-native/app-version';

import { ServiceProvider } from '../providers/service/service';

import { Events } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{title: string, component: any}>;
  nama:any;
  email:any;
  groupid:any;
  appversion:any;
  urlMaterial:any;
  photo:any;

  popUpUpdate:any;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen, 
    public storage:Storage,  
    public push: Push,
    public alertCtrl:AlertController,
    private toastCtrl: ToastController,
    private appVersion: AppVersion,
    public menuCtrl: MenuController,
    public service: ServiceProvider,
    public detectorRef: ChangeDetectorRef,
    public events: Events,
  ){
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Beranda', component: HomePage },
      //{ title: 'Katalog', component: KatalogPage },
    ];

    this.storage.get('user_data').then((val) => {
      this.nama = val.full_name;
      this.email = val.email;
      this.groupid = val.group.id;
      this.photo = val.photo;
    });

    this.events.subscribe('userdata', language => {
      this.storage.get('user_data').then((val) => {
        this.nama = val.full_name;
        this.email = val.email;
        this.groupid = val.group.id;
        this.photo = val.photo;
      });

      this.detectorRef.detectChanges();
    });

    this.appVersion.getVersionNumber().then(version => {
      this.appversion = version;
      console.log(this.appversion);
    });

    this.service.urlMaterial().then(url=>{
      this.urlMaterial = url;
    });

    this.storage.get('is_login').then((val) => {
      if(val==true){
        this.rootPage = HomePage;
      }else{
        this.rootPage = AuthStatePage;
      }
    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.pushsetup();
      this.popUpUpdate = false;
      this.checkAppVersion();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  checkAppVersion() {
    this.appVersion.getVersionNumber().then(version => {
      this.service.getAppVersion().then(res=>{
        if(version != res['data']){
          this.popUpUpdate = true;
          console.log("apk harus update");
        }else{
          this.popUpUpdate = false;
          console.log("apk tidak harus update");
        }
      });
    });
  }

  openPage(page){
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    let confirm = this.alertCtrl.create({
      title: 'Logout',
      message: 'Apakah anda yakin mau logout?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            
          }
        },
        {
          text: 'Ya',
          handler: () => {
            confirm.present();
            this.storage.set('user_data', null);
            this.storage.set('is_login', false);
            this.events.publish('userdata', null);
            this.nav.setRoot(LoginPage);
          }
        }
      ]
    });
    confirm.present();
  }

  pushsetup() {
    // to check if we have permission
    this.push.hasPermission()
    .then((res: any) => {
      if (res.isEnabled) {
        console.log('We have permission to send push notifications');
      } else {
        console.log('We don\'t have permission to send push notifications');
      }
    });

    // to initialize push notifications
    const options: PushOptions = {
      android: {
        senderID: '161691208016'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {}
    };
    const pushObject: PushObject = this.push.init(options);
      pushObject.on('notification').subscribe((notification: any) =>{
      console.log('Received a notification', notification);
      //Notification Display Section

      //set notif sales
      this.service.notifSalesBaru();

      /*if(notification.additionalData.tipe="new_sales"){
        textButton = "Lihat";
      }else{
        textButton = "Tutup";
      }*/

      console.log('notification',notification);

      if(notification.additionalData.tipe=='update_app'){
        //window.open("market://details?id=com.ahza.patria", "_system");
        this.checkAppVersion();
      }else{
        let toast = this.toastCtrl.create({
          message: notification.message,
          duration: 8000,
          position: 'bottom',
          showCloseButton: true,
          closeButtonText: 'Lihat'
        });

         toast.onDidDismiss((data, role) => {
          if (role == 'close') {
            if(typeof notification.additionalData.parram != 'undefined'){
              this.nav.push(notification.additionalData.page, notification.additionalData.parram);
            }
          }
        });
        toast.present();
      }
      //
    });

    pushObject.on('registration').
    subscribe((registration: any) => {
      console.log(registration);
      this.storage.set('fcm_token', registration.registrationId);
    });
    pushObject.on('error').
    subscribe(error => 
    console.error('Error with Push plugin', error));
  }

  goTo(link){
    this.menuCtrl.close();
    this.nav.push(link);
  }

  playstore(){
    window.open("market://details?id=com.ahza.patria", "_system");
  }
}

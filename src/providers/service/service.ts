import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';

@Injectable()
export class ServiceProvider {

  constructor(public http: Http, public events: Events) {
    
  }

  public baseUrl(){
    // const URL = 'http://apps.patria.co.id/api/';
    //const URL = 'https://patria.ahzadigital.net/api/';
     const URL = 'http://192.168.0.111/patria/api/';

    return URL;
  }

  public urlMaterial(){
    return new Promise(resolve => {
      // const url ="http://apps.patria.co.id/material/";
      //const url ="https://patria.ahzadigital.net/material/";
      const url ="http://192.168.0.111/patria/material/";

      resolve(url);
    });
  }


  public loginProcess(data){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/login';
      var myData = {
        identity:data.email,
        password:data.password,
      };
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
      	console.log(err);
        resolve({status:'500'});
      });
    });
  }


  public daftarProcess(data){
    
  	return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/user';
      var myData = {
        full_name:data.nama,
        phone:data.hp,
        bank:data.bank,
        no_rekening:data.rek,
        nik:data.nik,
        no_npwp:data.no_npwp,
        rekening_atas_nama:data.rek_nama,
        dealer:data.dealer,
        alamat_dealer:data.alamat_dealer,
        email:data.email,
        password:data.password,
        bc_name: data.nama_bc,
        signature: data.signature.replace('data:image/png;base64,','')
      };

      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        console.log('res daftar',data);
        if(data.status==true){
          resolve({status:'200', data: data.data});
        }else if(data.status==false){
          resolve({status:'300', data: data.message});
        }
      }, (err) =>{
      	console.log(err);
        resolve({status:'500'});
      });
    });	
  }

  public getAllSales(group_id){
    console.log('group_id get dealer',group_id);
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/all_user';
      var myData = {
        group_id:group_id,
      };
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log("Err sales patria", err);
        resolve({status:'500'});
      });
    }); 
  }

  public getAllSalesDealer(parent_id){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/all_user';
      var myData = {
        parent_id:parent_id,
      };
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log("Err sales dealer", err);
        resolve({status:'500'});
      });
    }); 
  }

  public getSalesById(id){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/user/id/'+id;
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  } 

  public konfirmasiUser(id, parent_id, active){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/user_appointing/id/'+id;
      let headers = new Headers({ 'Content-Type': 'application/json' });
      var myData = {
        parent_id:parent_id,
        active:active
      };
      this.http.put(endPoint, myData, {headers:headers})
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  }

  public pushNotifikasi(user_id, additional_data){
    let endPoint = this.baseUrl()+'device/push';
    var myData = {
      user_id:user_id,
      additional_data:additional_data
    };
    this.http.post(endPoint, myData)
    .map(response => response.json())
      .subscribe(data => {
        console.log('push status', data);
      }, (err) =>{
        console.log(err);
      });
  }

  public getSalesByParent(parent_id){
    console.log('parent_id',parent_id);
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/all_user_by_parent/parent_id/'+parent_id;
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        console.log('data parent_id', data);
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  }

  public fcmRegister(data){
    let endPoint = this.baseUrl()+'device/fcm_token';
    this.http.post(endPoint, data)
    .map(response => response.json())
    .subscribe(data => {
      console.log(data);
    }, (err) =>{
      console.log(err);
    });
  }


  public getAllProduk(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'katalog/produk';
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  }

  public getProdukAttribut(id){
   return new Promise(resolve => {
      let endPoint = this.baseUrl()+'katalog/produk_atribut/id/'+id;
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    });  
  }

  public notifSalesBaru(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/user_notif_new';
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        this.events.publish('notif_sales_baru', data.data); 
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  } 

  public getFaq(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'settings/faq';
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  } 

  public order(data){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'katalog/order';
      this.http.post(endPoint, data)
      .map(response => response.json())
      .subscribe(data => {
        this.events.publish('event_order', data); 
        resolve({status:'200', data: data.id});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public orderHistori(data){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'katalog/order_histori';
      this.http.post(endPoint, data)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.id});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public editProfil(id, data){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/user/id/'+id;
      let headers = new Headers({ 'Content-Type': 'application/json' });
      this.http.put(endPoint, data, {headers:headers})
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.message, ktp: data.ktp, npwp: data.npwp, tabungan: data.tabungan, signature: data.signature, surat_kesepakatan: data.surat_kesepakatan});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  }


  public getAllOrder(query){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'katalog/all_order';
      let myData =  {
        where: query
      }
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data});
      }, (err) =>{
        resolve({status:'500'});
      });
    });
  }

  public getAllOrderHistori(query){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'katalog/all_histori_order';
      let myData =  {
        where: query
      }
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  confirmOrder(data){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'katalog/order';
      let headers = new Headers({ 'Content-Type': 'application/json' });
      this.http.put(endPoint, data, {headers:headers})
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data});
      }, (err) =>{
        console.log(err);
      });
    }); 
  }


  public terms(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'settings/apps';
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public suratKesepakatanBersama(data){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'settings/surat_kesepakatan_bersama';
      let myData = {
        nama: data['nama'],
        nik: data['nik'],
        alamat_dealer: data['alamat_dealer'],
        bank: data['bank'],
        rek: data['rek'],
        rek_nama: data['rek_nama'],
        signature: data['signature']
      }
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public insertClaim(data){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'reward/claim_reword';
      this.http.post(endPoint, data)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public getRewardPoint(user_id){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'reward/saldo/user_id/'+user_id;
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        console.log(data);
        resolve({status:'200', data: data});
      }, (err) =>{
        console.log(err);
      });
    }); 
  }

  public rewardHostori(id_order){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'reward/histori/id_order/'+id_order;
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });  
  }

  public getPromo(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'settings/promo';
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });  
  }

  public createInbox(data){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'inbox/create_inbox';
      this.http.post(endPoint, data)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public getInbox(user_id){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'inbox/all_inbox';
      let myData = {
        where: "m_inbox.user_id_to='"+user_id+"' ORDER BY m_inbox.created_date DESC"
      }
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        resolve({status:'500'});
        console.log(err);
      });
    }); 
  }

  public getInboxDetil(id){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'inbox/all_inbox';
      let myData = {
        where: "m_inbox.id='"+id+"'"
      }
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });  
  }

  public getNotifInbox(user_id){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'inbox/all_inbox';
      let myData = {
        where: "m_inbox.user_id_to='"+user_id+"' and status='0'"
      }
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    }); 
  }

  public updateInbox(id){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'inbox/update_inbox/id/'+id;
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data});
      }, (err) =>{
        resolve(err);
      });
    });  
  }

  public getAllUserPatria(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/all_user';
      var myData = {
        group_id:4,
      };
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  }

  public getAllUserDealer(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/all_user';
      var myData = {
        group_id:5,
      };
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  }

  public getAllUserDealerChild(user_id){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/all_user';
      var myData = {
        parent_id:user_id,
      };
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  }

  public ubahSandi(data){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/change_password';
      this.http.post(endPoint, data)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.message, resp: data.status});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  }

  public getKantor(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'settings/kantor';
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public getAppVersion(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'settings/apps';
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data.android_version_release});
      }, (err) =>{
        console.log(err);
      });
    });
  }


  public getManager(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/manager';
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public getGeneralManager(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/general_manager';
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public getFinance(){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/finance';
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public cekProfile(user_id){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/cek_profile/user_id/'+user_id;
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public forgotPassword(identity){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'users/forget_password';
      let myData = {
        identity: identity
      }
      this.http.post(endPoint, myData)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data});
      }, (err) =>{
        console.log(err);
      });
    }); 
  }

  public getDetailPromo(id){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'settings/detail_promo/id/'+id;
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data});
      }, (err) =>{
        console.log(err);
      });
    });  
  }

  public UploadDokumen(type, file, user_id, full_name){
    return new Promise(resolve => {
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let endPoint = this.baseUrl()+'users/upload_dokumen';
      var myData = {
        type: type,
        file: file,
        user_id: user_id,
        full_name: full_name
      };
      console.log('UploadDokumen', myData);
      this.http.post(endPoint, myData, {headers:headers})
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', message: data.message, file: data.file});
      }, (err) =>{
        console.log(err);
        resolve({status:'500'});
      });
    }); 
  }

  public internalMemo(data){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'settings/internal_memo';
      this.http.post(endPoint, data)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public createMemoFile(data){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'katalog/create_memo_internal_file';
      this.http.post(endPoint, data)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });
  }

  public updateMemoFile(id){
    return new Promise(resolve => {
      let endPoint = this.baseUrl()+'katalog/update_memo_internal_file/id/'+id;
      this.http.get(endPoint)
      .map(response => response.json())
      .subscribe(data => {
        resolve({status:'200', data: data.data});
      }, (err) =>{
        console.log(err);
      });
    });
  }
}
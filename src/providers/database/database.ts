import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';

import { Storage } from '@ionic/storage';

@Injectable()
export class DatabaseProvider {

  constructor(
  	public http: Http,
  	public storage:Storage,
    public events: Events
  ){  
  	
  }

  public insertUser(data){
  	this.storage.set('user_data', data);
  	this.storage.set('is_login', true);
    this.events.publish('userdata', data);
  }

}

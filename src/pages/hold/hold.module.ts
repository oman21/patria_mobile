import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HoldPage } from './hold';

@NgModule({
  declarations: [
    HoldPage,
  ],
  imports: [
    IonicPageModule.forChild(HoldPage),
  ],
})
export class HoldPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the HoldPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hold',
  templateUrl: 'hold.html',
})
export class HoldPage {
	alasan:any;
	catatan:any;
	error:any = [];
	loader:any;

	items:any = [];

	histori:any =[];
	datauser:any = [];
	groupid:any;
  
  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public service: ServiceProvider,
  	public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public storage:Storage, 
  ) {

  	this.storage.get('user_data').then((val) 	=> {
  		this.datauser.push(val);
  		this.groupid = val.group.id;
  	});

  	let query = 't_order.id='+this.navParams.get('order_id');
  	this.service.getAllOrder(query).then(res=>{
  		this.items = (res['data']['data'][0]);
  	});


  	let queryHistory = 't_order_histori.id='+this.navParams.get('order_id');;
  	this.service.getAllOrderHistori(queryHistory).then(res=>{
  		for(let i=0;i<res['data']['data'].length;i++){
	  		this.histori.push(res['data']['data'][i]);
	  	}
  	});
  }

  confirm(){
  	console.log(this.catatan);
  	if(typeof this.alasan == 'undefined'){
  		this.error.catatan = '';
  		this.error.alasan = "Harus dipilih satu";
  	}else if(this.alasan == '3' && typeof this.catatan == 'undefined'){
  		this.error.alasan = '';
  		this.error.catatan = "Alasan lainnya harus diisi";
  	}else{
  		this.error.catatan = '';
  		this.error.alasan = '';
  		let confirm = this.alertCtrl.create({
      title: 'Pesanan Ditunda',
      message: 'Apakah anda yakin mau menunda pesanan ini?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.loader = this.loadingCtrl.create({
                content: 'Memproses...',
            });
	            this.loader.present();

	            this.confirm_process();  
	          }
	        }
	      ]
	    });
    	
    	confirm.present();
    }
  }

  confirm_process(){
    let data = {
      id: this.navParams.get('order_id'),
      status_order: "hold"
    }

    this.service.confirmOrder(data).then(res=>{
    	let ket = '';
    	if(this.alasan=='3'){
    		ket = this.catatan;
    	}else{
    		ket = this.alasan;
    	}

      let dataHistori = {
        id_order: this.navParams.get('order_id'),
        action_from: this.navParams.get('status_before'),
        action_to: 'hold',
        keterangan: ket,
        user_id: this.datauser[0]['user_id']
      }
      this.service.orderHistori(dataHistori).then(reshistori=>{
      
        let query = 't_order.id='+this.navParams.get('order_id');
        this.service.getAllOrder(query).then(res=>{
          let pushPatria = {
            data:{
              message: "Status - Order Hold",
                tipe: "order_hold",
                page: "PemesananDetilPage",
                parram: {
                  order_id:this.navParams.get('order_id')
                }
            }
          }

          this.service.pushNotifikasi(this.datauser[0]['parent_id'], pushPatria);

          let pushMgr = {
            data:{
              message: "Status - Order Hold",
                tipe: "order_hold",
                page: "PemesananDetilPage",
                parram: {
                  order_id:this.navParams.get('order_id')
                }
            }
          }

          this.service.pushNotifikasi(10, pushMgr);
        });
        
        this.loader.dismiss();

        this.navCtrl.pop();
        
        let alert = this.alertCtrl.create({
          title: 'Berhasil di hold',
          buttons: [
            {
              text: 'Tutup',
            }
          ]
        });
        alert.present();
      });
    })
  }
}

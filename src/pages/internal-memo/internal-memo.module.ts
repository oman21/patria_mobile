import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InternalMemoPage } from './internal-memo';

@NgModule({
  declarations: [
    InternalMemoPage,
  ],
  imports: [
    IonicPageModule.forChild(InternalMemoPage),
  ],
})
export class InternalMemoPageModule {}

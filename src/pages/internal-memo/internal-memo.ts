import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { DomSanitizer } from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-internal-memo',
  templateUrl: 'internal-memo.html',
})
export class InternalMemoPage {

  data:any;

  constructor(
    public navCtrl: NavController,
  	public navParams: NavParams,
  	public service: ServiceProvider,
  	protected sanitizer: DomSanitizer
  ) {
    var myData = this.navParams.get('data');
  	this.service.internalMemo(myData).then(res=>{
  		console.log("res", res);
  		this.data = this.sanitizer.bypassSecurityTrustHtml(res['data']);
  	});
  }
}

import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { Events } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-reward-detail',
  templateUrl: 'reward-detail.html',
})
export class RewardDetailPage {
	items:any = [];
	collect:any = [];
	modal:any;
	tabs:any;
	loader:any;
  groupid:any;
  histori:any = [];
  datauser:any = [];

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public storage:Storage,
    public service: ServiceProvider,
    public detectorRef: ChangeDetectorRef,
    public events: Events,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private inAppBrowser:InAppBrowser
  ){
    
  }

  ionViewWillEnter(){
    this.items = [];
    this.datauser = [];
    this.loader = this.loadingCtrl.create({
      content: 'Memproses...',
    });
    this.loader.present();

    this.tabs = 'detail';

    this.storage.get('user_data').then((val) 	=> {
      this.groupid = val.group.id;
      this.datauser.push(val);
    });

    let query = "t_order.id="+this.navParams.get("order_id");
      
    this.service.getAllOrder(query).then(res=>{
      console.log('getAllOrder', res);
      this.items.push(res['data']['data'][0]);
      this.loader.dismiss();
    });


    this.service.rewardHostori(this.navParams.get("order_id")).then(res=>{
      this.histori = res['data'];
    });
  }

  lihat_order(){
  	this.navCtrl.push("PemesananDetilPage",{order_id:this.navParams.get('order_id')});
  }

  internal_memo(pdf){
    this.service.urlMaterial().then(url=>{
      var options:any = {
        location: 'no',
        clearcache: 'yes'
      };

      var timestamp = new Date().getTime();
      this.inAppBrowser.create(url+'internal_memo/'+pdf+'?'+timestamp, '_system', options);
    });
  }

  verified(){
    let data = {
      data_claim: this.items[0],
      id_order: this.navParams.get('order_id'),
      status: 'verified',
      keterangan: 'Reward di verified, menunggu approval manager',
      created_date: moment().format('YYYY-MM-DD h:mm:s')
    }
    
    this.navCtrl.push('RewardVerificationPage', {data: data});
  }

  sendpushmgrClaim(idManager, dataInboxMgr){
    console.log("manager claim reward", idManager, dataInboxMgr);
    this.service.createInbox(dataInboxMgr).then(res_inbox=>{
      let pushMgr = {
        data:{
          message: "Claim Reward diverifikasi",
            tipe: "order_confirm",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
        }
      }

      this.service.pushNotifikasi(idManager, pushMgr);
    });
  }

  realese(){
    let alert = this.alertCtrl.create();
    alert.setTitle('Apakah anda yakin mau release claim ini?');

    alert.addButton('Batal');
    alert.addButton({
      text: 'Ya',
      handler: data => {
        this.realese_process();        
      }
    });
    alert.present();  
  }

  realese_process(){
  	this.loader = this.loadingCtrl.create({
	      content: 'Memproses...',
	  });
	  this.loader.present();

  	let data = {
  			user_id: this.items[0]['user_id'],
        id_order: this.navParams.get('order_id'),
        status: 'finish',
        keterangan: 'Reward telah di release',
        created_date: moment().format('YYYY-MM-DD h:mm:s')
		}

		this.service.insertClaim(data).then(res=>{console.log('insert claim', res);});

		let update = {
			id:this.items[0]['id'],
			is_claim_reward: 1
		}

		this.service.confirmOrder(update).then(res=>{
      this.loader.dismiss();
      
      let dataInboxDealer = {
        id_relation: this.items[0]['id'],
        tipe_relation: "RewardDetailPage",
        user_id_from: this.datauser[0]['user_id'],
        user_id_to: this.items[0]['user_id'],
        judul: "Reward telah di release",
        content: "Reward telah di release",
      }

      this.service.createInbox(dataInboxDealer).then(res_inbox=>{
        let pushDealer = {
          data:{
            message: "Reward telah di release",
              tipe: "claim_reward",
              page: "InboxDetilPage",
              parram: {
                inbox_id:res_inbox['data']['id']
              }
          }
        }

        this.service.pushNotifikasi(this.items[0]['user_id'], pushDealer);
      });
      
      this.service.getManager().then(resMgr=>{
        for(var i=0;i<resMgr['data'].length;i++){

          var idManager = resMgr['data'][i]['user_id'];

          let dataInboxMgr = {
            id_relation: this.items[0]['id'],
            tipe_relation: "RewardDetailPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: idManager,
            judul: "Reward telah di berikan",
            content: "Reward telah di berikan",
          }

          this.sendpushmgrReward(idManager, dataInboxMgr);
        }
      });  

      let alert = this.alertCtrl.create({
        title: 'Reward telah di berikan',
        buttons: [
          {
            text: 'Tutup',
          }
        ]
      });
      alert.present();

      this.ionViewWillEnter();
    });
  }

  sendpushmgrReward(idManager, dataInboxMgr){
    this.service.createInbox(dataInboxMgr).then(res_inbox=>{
      let pushMgr = {
        data:{
          message: "Reward telah di berikan",
            tipe: "order_confirm",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
        }
      }

      this.service.pushNotifikasi(idManager, pushMgr);
    });
  }

  reject(){
    let alert = this.alertCtrl.create();
    alert.setTitle('Apakah anda yakin mau reject claim ini?');

    alert.addButton('Batal');
    alert.addButton({
      text: 'Ya',
      handler: data => {
        this.reject_process();        
      }
    });
    alert.present();  
  }

  reject_process(){
    this.loader = this.loadingCtrl.create({
        content: 'Memproses...',
    });
    this.loader.present();

    let data = {
        user_id: this.items[0]['user_id'],
        id_order: this.navParams.get('order_id'),
        status: 'reject_patria',
        keterangan: 'Reward telah di riject oleh sales patria',
        created_date: moment().format('YYYY-MM-DD h:mm:s')
    }

    this.service.insertClaim(data).then(res=>{console.log('insert claim', res);});

    let update = {
      id:this.items[0]['id'],
      is_claim_reward: 0
    }

    this.service.confirmOrder(update).then(res=>{
      this.loader.dismiss();
      
      let dataInboxDealer = {
        id_relation: this.items[0]['id'],
        tipe_relation: "RewardDetailPage",
        user_id_from: this.datauser[0]['user_id'],
        user_id_to: this.items[0]['user_id'],
        judul: "Claim reward di tolak",
        content: "Claim reward di tolak",
      }

      this.service.createInbox(dataInboxDealer).then(res_inbox=>{
        let pushDealer = {
          data:{
            message: "Claim reward di tolak",
              tipe: "claim_reward",
              page: "InboxDetilPage",
              parram: {
                inbox_id:res_inbox['data']['id']
              }
          }
        }

        this.service.pushNotifikasi(this.items[0]['user_id'], pushDealer);
      });
        
      this.service.getManager().then(resMgr=>{
        for(var i=0;i<resMgr['data'].length;i++){

          var idManager = resMgr['data'][i]['user_id'];

          let dataInboxMgr = {
            id_relation: this.items[0]['id'],
            tipe_relation: "RewardDetailPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: idManager,
            judul: "Penolakan Claim Reward",
            content: "Claim reward di tolak",
          }

          this.sendpushmgrRewardReject(idManager, dataInboxMgr);
        }
      });

      let alert = this.alertCtrl.create({
        title: 'Reward telah di berikan',
        buttons: [
          {
            text: 'Tutup',
          }
        ]
      });
      alert.present();

      this.ionViewWillEnter();
    });
  }

  sendpushmgrRewardReject(idManager, dataInboxMgr){
    this.service.createInbox(dataInboxMgr).then(res_inbox=>{
      let pushMgr = {
        data:{
          message: "Penolakan Claim Reward",
            tipe: "order_confirm",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
        }
      }

      this.service.pushNotifikasi(idManager, pushMgr);
    });
  }

  verified_realese(){
    if(this.datauser[0].signature==null){
      return false;
    }
    let alert = this.alertCtrl.create();
    alert.setTitle('Apakah anda yakin mau verifikasi release claim ini?');

    alert.addButton('Batal');
    alert.addButton({
      text: 'Ya',
      handler: data => {
        this.verified_realese_process();        
      }
    });
    alert.present();
  }

  verified_realese_process(){
    this.loader = this.loadingCtrl.create({
        content: 'Memproses...',
    });
    this.loader.present();

    let data = {
        user_id: this.items[0]['user_id'],
        id_order: this.navParams.get('order_id'),
        status: 'verified_gm',
        keterangan: 'Reward di verified, menunggu approval General Manager'
    }

    this.service.insertClaim(data).then(res=>{console.log('insert claim', res);});

    let update = {
      id:this.items[0]['id'],
      is_claim_reward: 5,
      claim_verified_sm_id: this.datauser[0]['user_id'],
      claim_verified_sm_name: this.datauser[0]['full_name'],
    }

    this.service.confirmOrder(update).then(res=>{
      this.loader.dismiss();
      
      let dataInboxDealer = {
        id_relation: this.items[0]['id'],
        tipe_relation: "RewardDetailPage",
        user_id_from: this.datauser[0]['user_id'],
        user_id_to: this.items[0]['user_id'],
        judul: "Internal Memo Approval Request",
        content: "Internal memo telah diverifikasi oleh manager, menunggu approval general manager",
      }

      this.service.createInbox(dataInboxDealer).then(res_inbox=>{
        let pushDealer = {
          data:{
            message: "Internal Memo Approval Request",
            tipe: "claim_reward",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
          }
        }

        this.service.pushNotifikasi(this.items[0]['user_id'], pushDealer);
      });

      //ke sales bc
      let dataInboxBc = {
        id_relation: this.items[0]['id'],
        tipe_relation: "RewardDetailPage",
        user_id_from: this.datauser[0]['user_id'],
        user_id_to: this.items[0]['claim_verified_bc_id'],
        judul: "Internal memo telah diverivikasi",
        content: "Internal memo telah diverifikasi oleh manager, menunggu approval general manager",
      }

      this.service.createInbox(dataInboxBc).then(res_inbox=>{
        let pushDealer = {
          data:{
            message: "Internal memo telah diverivikasi",
            tipe: "claim_reward",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
          }
        }

        this.service.pushNotifikasi(this.items[0]['claim_verified_bc_id'], pushDealer);
      });
      
      //ke general manager
      let id_relation = this.items[0]['id'];
      let user_id = this.datauser[0]['user_id'];
      this.service.getGeneralManager().then(resMgr=>{
        console.log('getGeneralManager', resMgr);
        console.log('this.items[0]', this.items[0]);
        
        for(var i=0;i<resMgr['data'].length;i++){
          let dataInboxPatria = {
            id_relation: id_relation,
            tipe_relation: "RewardDetailPage",
            user_id_from: user_id,
            user_id_to: resMgr['data'][i]['user_id'],
            judul: "Release reward telah diverifikasi",
            content: "Release reward telah diverifikasi oleh manager, silahkan verifikasi reward yang diajukan",
          }

          this.service.createInbox(dataInboxPatria).then(res_inbox=>{
            console.log('createInbox', res_inbox);
            let pushMgr = {
              data:{
                message: "Release reward telah diverifikasi",
                  tipe: "order_confirm",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(resMgr['data'][i]['user_id'], pushMgr);
          });  
        }
      })

      let alert = this.alertCtrl.create({
        title: 'Reward telah di verifikasi',
        buttons: [
          {
            text: 'Tutup',
          }
        ]
      });
      alert.present();

      this.ionViewWillEnter();
    });
  }

  realese_gm(){
    let alert = this.alertCtrl.create();
    alert.setTitle('Apakah anda yakin mau verifikasi release claim ini?');

    alert.addButton('Batal');
    alert.addButton({
      text: 'Ya',
      handler: data => {
        this.verified_gm_process();        
      }
    });
    alert.present();
  }

  verified_gm_process(){
    this.loader = this.loadingCtrl.create({
      content: 'Memproses...',
    });
    this.loader.present();

    let data = {
        user_id: this.items[0]['user_id'],
        id_order: this.navParams.get('order_id'),
        status: 'verified_finance',
        keterangan: 'Internal memo di verified, menunggu approval Finance'
    }

    this.service.insertClaim(data).then(res=>{console.log('insert claim', res);});

    let update = {
      id:this.items[0]['id'],
      is_claim_reward: 4,
      claim_verified_gm_id: this.datauser[0]['user_id'],
      claim_verified_gm_name: this.datauser[0]['full_name'],
    }

    this.service.confirmOrder(update).then(res=>{
      this.loader.dismiss();

      //create memo
      this.service.createMemoFile({order_id: this.navParams.get('order_id')})
      
      //send to sales dealer
      let dataInboxDealer = {
        id_relation: this.items[0]['id'],
        tipe_relation: "RewardDetailPage",
        user_id_from: this.datauser[0]['user_id'],
        user_id_to: this.items[0]['user_id'],
        judul: "Internal memo telah diverifikasi",
        content: "Internal memo telah diverifikasi oleh general manager, menunggu approval finance",
      }

      this.service.createInbox(dataInboxDealer).then(res_inbox=>{
        let pushDealer = {
          data:{
            message: "Internal mmemo telah diverifikasi",
            tipe: "claim_reward",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
          }
        }

        this.service.pushNotifikasi(this.items[0]['user_id'], pushDealer);
      });

      //ke sales bc
      let dataInboxBc = {
        id_relation: this.items[0]['id'],
        tipe_relation: "RewardDetailPage",
        user_id_from: this.datauser[0]['user_id'],
        user_id_to: this.items[0]['claim_verified_bc_id'],
        judul: "Internal memo telah diverivikasi",
        content: "Internal memo telah diverifikasi oleh general manager, menunggu approval finance",
      }

      this.service.createInbox(dataInboxBc).then(res_inbox=>{
        let pushDealer = {
          data:{
            message: "Internal memo telah diverivikasi",
            tipe: "claim_reward",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
          }
        }

        this.service.pushNotifikasi(this.items[0]['claim_verified_bc_id'], pushDealer);
      });

      //ke sales sm
      let dataInboxSM = {
        id_relation: this.items[0]['id'],
        tipe_relation: "RewardDetailPage",
        user_id_from: this.datauser[0]['user_id'],
        user_id_to: this.items[0]['claim_verified_sm_id'],
        judul: "Internal memo telah diverivikasi",
        content: "Internal memo telah diverifikasi oleh general manager, menunggu approval finance",
      }

      this.service.createInbox(dataInboxSM).then(res_inbox=>{
        let pushDealer = {
          data:{
            message: "Internal memo telah diverivikasi",
            tipe: "claim_reward",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
          }
        }

        this.service.pushNotifikasi(this.items[0]['claim_verified_sm_id'], pushDealer);
      });
      
      //ke general finance
      let id_relation = this.items[0]['id'];
      let user_id = this.datauser[0]['user_id'];
      this.service.getFinance().then(resMgr=>{
        
        for(var i=0;i<resMgr['data'].length;i++){
          let dataInboxPatria = {
            id_relation: id_relation,
            tipe_relation: "RewardDetailPage",
            user_id_from: user_id,
            user_id_to: resMgr['data'][i]['user_id'],
            judul: "Release reward telah diverifikasi",
            content: "Release reward telah diverifikasi oleh manager, silahkan verifikasi reward yang diajukan",
          }

          this.service.createInbox(dataInboxPatria).then(res_inbox=>{
            console.log('createInbox', res_inbox);
            let pushMgr = {
              data:{
                message: "Release reward telah diverifikasi",
                  tipe: "order_confirm",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(resMgr['data'][i]['user_id'], pushMgr);
          });  
        }
      })

      let alert = this.alertCtrl.create({
        title: 'Reward telah di verifikasi',
        buttons: [
          {
            text: 'Tutup',
          }
        ]
      });
      alert.present();

      this.ionViewWillEnter();
    });
  }

  edit_profile(){
    this.navCtrl.push("EditProfilPage");
  }

  internal_memo_sm(){
    let data_memo = {
      sm_name: this.datauser[0].full_name,
      order_id: this.navParams.get("order_id")
    }

    this.navCtrl.push("InternalMemoPage", {data:data_memo});
  }

  internal_memo_gm(){
    let data_memo = {
      order_id: this.navParams.get("order_id")
    }

    this.navCtrl.push("InternalMemoPage", {data:data_memo});
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RewardDetailPage } from './reward-detail';

@NgModule({
  declarations: [
    RewardDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(RewardDetailPage),
  ],
})
export class RewardDetailPageModule {}

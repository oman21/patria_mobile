import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';

/**
 * Generated class for the PemesananDetilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-order',
  templateUrl: 'confirm-order.html',
})
export class ConfirmOrderPage {
	items:any = [];

	histori:any =[];
	datauser:any = [];
	groupid:any;
  judul:any;
  catatan:any;
  err_catatan:any;

  loader:any;
  nextstatus:any;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public storage:Storage, 
  	public service: ServiceProvider,
  	public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
  ) {

    this.catatan = '';
    this.err_catatan = '';

    if(this.navParams.get('status_before')=='waiting'){
      this.judul = "Reject";
      this.nextstatus = "rejected";
    }else if(this.navParams.get('status_before')=='po_process'){
      this.judul = "Reject PO";
      this.nextstatus = "rejected_po";
    }

  	this.storage.get('user_data').then((val) 	=> {
  		this.datauser.push(val);
  		this.groupid = val.group.id;
  	});

  	let query = 't_order.id='+this.navParams.get('order_id');
  	this.service.getAllOrder(query).then(res=>{
  		this.items = (res['data']['data'][0]);
      console.log(this.items);
  	});
  }

  confirm(){
  	if(this.catatan == ''){
      this.err_catatan = 'Harus diisi!';
    }else{
      let confirm = this.alertCtrl.create({
      title: 'Reject Order',
      message: 'Apakah anda yakin mau '+this.judul+' orderan ini?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.loader = this.loadingCtrl.create({
                content: 'Memproses...',
            });
            this.loader.present();

            this.confirm_process();  
          }
        }
      ]
    });
    confirm.present();
    }
  }

  confirm_process(){
    let data = {
      id: this.navParams.get('order_id'),
      status_order: this.nextstatus
    }

    this.service.confirmOrder(data).then(res=>{
      let dataHistori = {
        id_order: this.navParams.get('order_id'),
        action_from: this.navParams.get('status_before'),
        action_to: this.nextstatus,
        keterangan: this.catatan,
        user_id: this.datauser[0]['user_id']
      }
      this.service.orderHistori(dataHistori).then(reshistori=>{
      
        let query = 't_order.id='+this.navParams.get('order_id');
        this.service.getAllOrder(query).then(res=>{

          let dataInboxPatria = {
            id_relation: this.navParams.get('order_id'),
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: res['data']['data'][0]['user_id'],
            judul: "Order Rejected",
            content: "Order anda di reject, klik Lihat untuk detilnya",
          }

          this.service.createInbox(dataInboxPatria).then(res_inbox=>{
            let pushPatria = {
              data:{
                message: "Order Reject",
                  tipe: "order_rejected",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(res['data']['data'][0]['user_id'], pushPatria);
          });

           let dataInboxMgr = {
            id_relation: this.navParams.get('order_id'),
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: this.datauser[0]['parent_id'],
            judul: "Order Rejected",
            content: "Salah satu orderan di reject, lihat detilnya",
          }

          this.service.createInbox(dataInboxMgr).then(res_inbox=>{
            let pushMgr = {
              data:{
                message: "Order Rejected",
                  tipe: "order_rejected",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(this.datauser[0]['parent_id'], pushMgr);
          });
        });
        
        this.loader.dismiss();

        this.navCtrl.pop();
        
        let alert = this.alertCtrl.create({
          title: 'Berhasil di '+this.judul+' pesanan',
          buttons: [
            {
              text: 'Tutup',
            }
          ]
        });
        alert.present();
      });
    })
  }

}

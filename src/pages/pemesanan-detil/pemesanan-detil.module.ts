import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PemesananDetilPage } from './pemesanan-detil';

@NgModule({
  declarations: [
    PemesananDetilPage,
  ],
  imports: [
    IonicPageModule.forChild(PemesananDetilPage),
  ],
})
export class PemesananDetilPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ActionSheetController  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { Events } from 'ionic-angular';
import {FormControl} from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-pemesanan-detil',
  templateUrl: 'pemesanan-detil.html',
})
export class PemesananDetilPage {
	tabs:any;
	items:any = [];

	histori:any =[];
	datauser:any = [];
	groupid:any;

	loader:any;

  request_price:any;
  request_note:any;
  requestPriceControl = new FormControl();
  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";
  data_error:any = [];

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public storage:Storage, 
  	public service: ServiceProvider,
  	public alertCtrl: AlertController,
  	public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public events: Events
  ) {

  	this.storage.get('user_data').then((val) 	=> {
  		this.datauser.push(val);
  		this.groupid = val.group.id;
  	});

  	this.tabs = 'detil';
    this.request_price = null;
    this.request_note = null;
    this.data_error.request_price = '';
    this.data_error.request_note = '';
  }

  ionViewWillEnter() {
    this.loader = this.loadingCtrl.create({
        content: 'Memproses...',
    });
    this.loader.present();
    this.getHistori();
  }

  getHistori(){
    this.histori = [];

    let query = 't_order.id='+this.navParams.get('order_id');
    this.service.getAllOrder(query).then(res=>{
      this.items = (res['data']['data'][0]);
      this.loader.dismiss();

      console.log(this.items);
    });

    let queryHistory = 't_order_histori.id_order='+this.navParams.get('order_id');
    this.service.getAllOrderHistori(queryHistory).then(res=>{
      for(let i=0;i<res['data']['data'].length;i++){
        this.histori.push(res['data']['data'][i]);
      }
    });
  }

  sales_detail(user_id){
    this.navCtrl.push('SalesDetailPage', {user_id:user_id});
  }

  confirm_order(id, request){
    console.log('request',request);
    if(request!=''){
      if(this.request_note==null||this.request_price==null){

        let confirm = this.alertCtrl.create({
          title: 'Pengisian belum lengkap!',
          buttons: [{text: 'Ok'}]
        });
        confirm.present();

        if(this.request_note==null){
          this.data_error.request_note = "harus diisi!";
        }else{
          this.data_error.request_note
        }

        if(this.request_price==null){
          this.data_error.request_price = "Harus diisi!";
        }else{
          this.data_error.request_price = "";
        }
      }else{
        this.data_error.request_price = "";
        this.data_error.request_note = "";
        this.confirm_order_request(id);  
      }
    }else{
      this.confirm_order_request(id);
    }
  }

  confirm_order_request(id){
    let confirm = this.alertCtrl.create({
      title: 'Verifikasi Order',
      message: 'Apakah anda yakin mau memverifikasi orderan ini?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.loader = this.loadingCtrl.create({
                content: 'Memproses...',
            });
            this.loader.present();

            this.confirm_order_process(id);  
          }
        }
      ]
    });
    confirm.present();
  }

  confirm_order_process(id){
    let data = {
      id: id,
      status_order: "verified",
      request_price: parseInt(this.unFormat(this.request_price)),
      request_feedback: this.request_note
    }

    this.service.confirmOrder(data).then(res=>{
      let dataHistori = {
        id_order: id,
        action_from: 'waiting',
        action_to: 'verified',
        keterangan: 'Order anda sudah di verifikasi, silahkan kirimkan PO anda atau pilih option berikut (option/button untuk hold order, cancel order , verifikasi PO telah dikirim)',
        user_id: this.datauser[0]['user_id']
      }
      this.service.orderHistori(dataHistori).then(reshistori=>{
      
        let query = 't_order.id='+this.navParams.get('order_id');
        this.service.getAllOrder(query).then(res=>{

          let dataInboxPatria = {
            id_relation: id,
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: res['data']['data'][0]['user_id'],
            judul: "Confirm Order",
            content: "Order anda sudah di verifikasi, silahkan kirimkan PO anda atau pilih option berikut (option/button untuk hold order, cancel order , verifikasi PO telah dikirim)",
          }

          this.service.createInbox(dataInboxPatria).then(res_inbox=>{
            let pushPatria = {
              data:{
                message: "Confirm Order",
                  tipe: "order_confirm",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(res['data']['data'][0]['user_id'], pushPatria);
          });

          let dataInboxMgr = {
            id_relation: id,
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: this.datauser[0]['parent_id'],
            judul: "Confirm Order",
            content: "Orderan diverifikasi sales",
          }

          this.service.createInbox(dataInboxMgr).then(res_inbox=>{
            let pushMgr = {
              data:{
                message: "Orderan diverifikasi sales",
                  tipe: "order_confirm",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(this.datauser[0]['parent_id'], pushMgr);
          });
        });
        
        this.loader.dismiss();

        this.histori =[];
        this.getHistori();

        let alert = this.alertCtrl.create({
          title: 'Berhasil di verifikasi',
          subTitle: 'Silahkan follow up sales anda',
          buttons: [
            {
              text: 'Tutup',
            }
          ]
        });
        alert.present();
      });
    })
  }

  reject_order(id, status_before){
    this.navCtrl.push("ConfirmOrderPage", {order_id:id, status_before:status_before});
  }

  change_status(id, status_before){
    if(status_before=='hold'){
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Ubah Status Order',
        buttons: [
          {
            text: 'PO Terkirim',
            handler: () => {
              this.kirim_po_ulang(id, status_before);
            }
          },{
            text: 'Cancel',
            handler: () => {
              this.navCtrl.push("CancelPage", {order_id:id, status_before: status_before});
            }
          }
        ]
      });
      actionSheet.present();
    }else if(status_before=='rejected_po'){
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Ubah Status Order',
        buttons: [
          {
            text: 'Kirim Ulang PO',
            handler: () => {
              this.kirim_po_ulang(id, status_before);
            }
          },{
            text: 'Hold',
            handler: () => {
              this.navCtrl.push("HoldPage", {order_id:id, status_before: status_before});
            }
          },{
            text: 'Cancel',
            handler: () => {
              this.navCtrl.push("CancelPage", {order_id:id, status_before: status_before});
            }
          }
        ]
      });
      actionSheet.present();
    }else{
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Ubah Status Order',
        buttons: [
          {
            text: 'PO Terkirim',
            handler: () => {
              this.kirim_po_ulang(id, status_before);
            }
          },{
            text: 'Hold',
            handler: () => {
              this.navCtrl.push("HoldPage", {order_id:id, status_before: status_before});
            }
          },{
            text: 'Cancel',
            handler: () => {
              this.navCtrl.push("CancelPage", {order_id:id, status_before: status_before});
            }
          }
        ]
      });
      actionSheet.present();
    }
  }

  kirim_po_ulang(id, status_before){
    this.navCtrl.push('PoPage', {id:id, status:status_before})
  }

  kirim_po(id, status_before){
    let confirm = this.alertCtrl.create({
      title: 'PO Terkirim',
      message: 'Apakah anda yakin PO terkirim untuk orderan ini?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.loader = this.loadingCtrl.create({
                content: 'Memproses...',
            });
            this.loader.present();

            this.kirim_po_process(id, status_before);  
          }
        }
      ]
    });
    confirm.present();
  }

  kirim_po_process(id, status_before){
    let data = {
      id: id,
      status_order: "po_process"
    }

    this.service.confirmOrder(data).then(res=>{
      let dataHistori = {
        id_order: id,
        action_from: status_before,
        action_to: 'po_process',
        keterangan: 'Pengubahan status PO Terkirim',
        user_id: this.datauser[0]['user_id']
      }
      this.service.orderHistori(dataHistori).then(reshistori=>{
      
        let query = 't_order.id='+this.navParams.get('order_id');
        this.service.getAllOrder(query).then(res=>{

          let dataInboxPatria = {
            id_relation: id,
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: this.datauser[0]['parent_id'],
            judul: "PO Terkirim",
            content: "Segera Process order dari sales anda. (PO Received)",
          }

          this.service.createInbox(dataInboxPatria).then(res_inbox=>{
            let pushPatria = {
              data:{
                message: "PO Terkirim",
                  tipe: "order_po_terkirim",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(this.datauser[0]['parent_id'], pushPatria);
          });
          
          this.service.getManager().then(resMgr=>{
            for(var i=0;i<resMgr['data'].length;i++){
              
              var idManager = resMgr['data'][i]['user_id'];

              let dataInboxMgr = {
                id_relation: id,
                tipe_relation: "PemesananDetilPage",
                user_id_from: this.datauser[0]['user_id'],
                user_id_to: idManager,
                judul: "PO Received - Belum di proses",
                content: "PO Received - Belum di proses oleh sales patria",
              }

              this.sendpushmgr(idManager, dataInboxMgr);
            }
          });
        });
        
        this.loader.dismiss();

        this.histori =[];
        this.getHistori();

        let alert = this.alertCtrl.create({
          title: 'Berhasil mengubah status ke PO Terkirim',
          subTitle: 'Order anda akan segera kami proses',
          buttons: [
            {
              text: 'Tutup',
            }
          ]
        });
        alert.present();
      });
    })
  }

  sendpushmgr(idManager, dataInboxMgr){
    this.service.createInbox(dataInboxMgr).then(res_inbox=>{
      let pushMgr = {
        data:{
          message: "PO Received - Belum di proses",
            tipe: "order_confirm",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
        }
      }

      this.service.pushNotifikasi(idManager, pushMgr);
    });
  }

  confirm_po(id){
    let confirm = this.alertCtrl.create({
      title: 'Verifikasi PO',
      message: 'Apakah anda yakin mau memverifikasi PO orderan ini?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.loader = this.loadingCtrl.create({
                content: 'Memproses...',
            });
            this.loader.present();

            this.confirm_po_process(id);  
          }
        }
      ]
    });
    confirm.present();
  }

  confirm_po_process(id){
    let data = {
      id: id,
      status_order: "on_process"
    }

    this.service.confirmOrder(data).then(res=>{
      let dataHistori = {
        id_order: id,
        action_from: 'po_process',
        action_to: 'on_process',
        keterangan: 'Order Sedang diproses',
        user_id: this.datauser[0]['user_id']
      }
      this.service.orderHistori(dataHistori).then(reshistori=>{
      
        let query = 't_order.id='+this.navParams.get('order_id');
        this.service.getAllOrder(query).then(res=>{

          let dataInboxPatria = {
            id_relation: id,
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: res['data']['data'][0]['user_id'],
            judul: "Order Sedang diproses",
            content: "Order Sedang diproses",
          }

          this.service.createInbox(dataInboxPatria).then(res_inbox=>{
            let pushPatria = {
              data:{
                message: "Order Sedang diproses",
                  tipe: "order_confirm",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(res['data']['data'][0]['user_id'], pushPatria);
          });
          
          let dataInboxMgr = {
            id_relation: id,
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: this.datauser[0]['parent_id'],
            judul: "Order On-Process.",
            content: "Order On-Process.",
          }

          this.service.createInbox(dataInboxMgr).then(res_inbox=>{
            let pushMgr = {
              data:{
                message: "Order On-Process",
                  tipe: "order_confirm",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(this.datauser[0]['parent_id'], pushMgr);
          });
        });
        
        this.loader.dismiss();

        this.histori =[];
        this.getHistori();

        let alert = this.alertCtrl.create({
          title: 'Berhasil di verifikasi',
          subTitle: 'Order Sedang diproses, silahkan cek progress order dan pembayaran.',
          buttons: [
            {
              text: 'Tutup',
            }
          ]
        });
        alert.present();
      });
    })
  }

  delivery(id, status_before){
    let confirm = this.alertCtrl.create({
      title: 'Verifikasi Order',
      message: 'Ubah status ke Delivered?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.loader = this.loadingCtrl.create({
                content: 'Memproses...',
            });
            this.loader.present();

            this.confirm_delivery_process(id);  
          }
        }
      ]
    });
    confirm.present();
  }

  confirm_delivery_process(id){
    let data = {
      id: id,
      status_order: "delivered"
    }

    this.service.confirmOrder(data).then(res=>{
      let dataHistori = {
        id_order: id,
        action_from: 'on_process',
        action_to: 'delivered',
        keterangan: 'Order Anda sedang dalam pengiriman - Hubungi sales anda jika reward belum release',
        user_id: this.datauser[0]['user_id']
      }
      this.service.orderHistori(dataHistori).then(reshistori=>{
      
        let query = 't_order.id='+this.navParams.get('order_id');
        this.service.getAllOrder(query).then(res=>{

          let dataInboxPatria = {
            id_relation: id,
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: res['data']['data'][0]['user_id'],
            judul: "Order Dalam Pengiriman",
            content: "Order Anda sedang dalam pengiriman - Hubungi sales anda jika reward belum release",
          }

          this.service.createInbox(dataInboxPatria).then(res_inbox=>{
            let pushPatria = {
              data:{
                message: "Order Dalam Pengiriman",
                  tipe: "order_confirm",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(res['data']['data'][0]['user_id'], pushPatria);
          });

          let dataInboxMgr = {
            id_relation: id,
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: this.datauser[0]['parent_id'],
            judul: "Status Order Delivered",
            content: "Status Order Delivered - Reward Released / Reward Belum Release",
          }

          this.service.createInbox(dataInboxMgr).then(res_inbox=>{
            let pushMgr = {
              data:{
                message: "Status Order Delivered",
                  tipe: "order_confirm",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(this.datauser[0]['parent_id'], pushMgr);
          });
        });
        
        this.loader.dismiss();

        this.histori =[];
        this.getHistori();

        let alert = this.alertCtrl.create({
          title: 'Berhasil di verifikasi',
          subTitle: 'Status Order Delivered - Silahkan Release Reward untuk Sales anda',
          buttons: [
            {
              text: 'Tutup',
            }
          ]
        });
        alert.present();
      });
    })
  }

  delivery_cancel(id, status_before){
    this.navCtrl.push("CancelPage", {order_id:id, status_before: status_before});
  }

  req_release_reward(id){
    let confirm = this.alertCtrl.create({
      title: 'Verifikasi Order',
      message: 'Release Reward?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.loader = this.loadingCtrl.create({
                content: 'Memproses...',
            });
            this.loader.present();

            this.confirm_req_release_reward_process(id);  
          }
        }
      ]
    });
    confirm.present();
  }

  confirm_req_release_reward_process(id){
    let data = {
      id: id,
      status_order: "req_released_reward"
    }

    this.service.confirmOrder(data).then(res=>{
      let dataHistori = {
        id_order: id,
        action_from: 'delivered',
        action_to: 'req_released_reward',
        keterangan: 'Produk sudah dikirim, release reward sedang dalam proses',
        user_id: this.datauser[0]['user_id']
      }
      this.service.orderHistori(dataHistori).then(reshistori=>{
        
        let query = 't_order.id='+this.navParams.get('order_id');
        this.service.getAllOrder(query).then(res=>{

          let dataInboxPatria = {
            id_relation: id,
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: res['data']['data'][0]['user_id'],
            judul: "Release Reward Sedang Diproses",
            content: "Produk sudah dikirim, release reward sedang dalam proses",
          }

          this.service.createInbox(dataInboxPatria).then(res_inbox=>{
            let pushPatria = {
              data:{
                message: "Produk sudah dikirim",
                  tipe: "order_confirm",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }
            this.service.pushNotifikasi(res['data']['data'][0]['user_id'], pushPatria);
          });
          
          let dataInboxMgr = {
            id_relation: id,
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: this.datauser[0]['parent_id'],
            judul: "Verifikasi Release Reward",
            content: "Order Sudah dikirim, Silahkan Verifikasi untuk Release Reward",
          }

          this.service.createInbox(dataInboxMgr).then(res_inbox=>{
            let pushMgr = {
              data:{
                message: "Order Sudah dikirim",
                  tipe: "order_confirm",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(this.datauser[0]['parent_id'], pushMgr);
          });
        });
        
        this.loader.dismiss();

        this.histori =[];
        this.getHistori();

        let alert = this.alertCtrl.create({
          title: 'Request Release Reward Berhasil',
          subTitle: 'Menunggu Verifikasi Release Reward dari sales manager',
          buttons: [
            {
              text: 'Tutup',
            }
          ]
        });
        alert.present();
      });
    })
  }

  approve_reward(id){
    let confirm = this.alertCtrl.create({
      title: 'Approved Reward',
      message: 'Release Reward?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.loader = this.loadingCtrl.create({
                content: 'Memproses...',
            });
            this.loader.present();

            this.confirm_approve_reward_process(id);  
          }
        }
      ]
    });
    confirm.present();
  }

  confirm_approve_reward_process(id){
    let data = {
      id: id,
      status_order: "finish"
    }

    this.service.confirmOrder(data).then(res=>{
      let dataHistori = {
        id_order: id,
        action_from: 'req_released_reward',
        action_to: 'finish',
        keterangan: 'Order Finish - Reward sudah Release',
        user_id: this.datauser[0]['user_id']
      }
      this.service.orderHistori(dataHistori).then(reshistori=>{
      
        let query = 't_order.id='+this.navParams.get('order_id');
        this.service.getAllOrder(query).then(res=>{

          let dataInboxPatria = {
            id_relation: id,
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: res['data']['data'][0]['user_id'],
            judul: "Order Finish",
            content: "Order Finish - Reward sudah Release",
          }

          this.service.createInbox(dataInboxPatria).then(res_inbox=>{
            let pushPatria = {
              data:{
                message: "Order Finish - Reward sudah Release",
                  tipe: "order_confirm",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(res['data']['data'][0]['user_id'], pushPatria);
          });

          this.service.getSalesById(res['data']['data'][0]['user_id']).then(resSales=>{
            let dataInboxDealer = {
              id_relation: id,
              tipe_relation: "PemesananDetilPage",
              user_id_from: this.datauser[0]['user_id'],
              user_id_to: resSales['data']['parent_id'],
              judul: "Order Finish",
              content: "Order Finish - Reward sudah Release",
            }

            this.service.createInbox(dataInboxDealer).then(res_inbox=>{
              let pushDealer = {
                data:{
                  message: "Order Finish - Reward sudah Release",
                    tipe: "order_confirm",
                    page: "InboxDetilPage",
                    parram: {
                      inbox_id:res_inbox['data']['id']
                    }
                }
              }

              this.service.pushNotifikasi(resSales['data']['parent_id'], pushDealer);
            });
          });
        });
        
        this.loader.dismiss();

        this.events.publish('reward_point', reshistori);

        this.histori =[];
        this.getHistori();

        let alert = this.alertCtrl.create({
          title: 'Berhasil di verifikasi',
          subTitle: 'Reward berhasil direlease',
          buttons: [
            {
              text: 'Tutup',
            }
          ]
        });
        alert.present();
      });
    })
  }

  format(valString) {
    if (!valString) {
      return '';
    }
    let val = valString.toString();
    const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);

    var new_format = parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR) + (!parts[1] ? '' : this.DECIMAL_SEPARATOR + parts[1]);

    return new_format;
  };

  unFormat(val) {
    if (!val) {
        return '';
    }
    val = val.replace(/^0+/, '');

    if (this.GROUP_SEPARATOR === ',') {
        return val.replace(/,/g, '');
    } else {
        return val.replace(/\./g, '');
    }
  }

  total(request, estimasi){
    if(typeof request=='undefined'||request==0||request==''){
      request = 0;
    }
    return parseInt(request)+parseInt(estimasi);
  }
}

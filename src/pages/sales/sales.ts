import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the SalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sales',
  templateUrl: 'sales.html',
})
export class SalesPage {
	tabssales:any;
	filter:any = [];
	sales:any;
	salesdealer:any;
  salesdealerfilter:any;
  salesdealerfull:any=[];
	urlMaterial:any;
	group_id:any;
  userdata:any = [];
  loader:any;
  searchKey:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public service:ServiceProvider, public storage:Storage, public loadingCtrl: LoadingController,) {
  	this.filter.verified = true;
  	this.filter.waiting = true;
  	
  	this.service.urlMaterial().then(url=>{
  		this.urlMaterial = url;
  	});

  	this.storage.get('user_data').then((val) => {
  		this.group_id = val.group.id;
      if(val.group.id==3){
      	this.tabssales = 'patria';
    	}else{
    		this.tabssales = 'dealer';
    	}
    })
  }

  ionViewWillEnter() {

    this.filter[0] = true;
    this.filter[1] = true;

    this.loader = this.loadingCtrl.create({
        content: 'Memproses...',
    });
    this.loader.present();

  	this.sales = [];
  	this.salesdealer = [];
    this.salesdealerfilter = [];

    this.storage.get('user_data').then((val) => {
      if(val.group.id==3){
      	this.getSales(4);
      	this.getSalesDealer(5);
        this.loader.dismiss();
      }else{
        this.getSalesDealerPatria(val.user_id);
        this.loader.dismiss();
      }
    });
  }

  getSales(group_id){
  	this.service.getAllSales(group_id).then(res=>{
  		for(let i=0;i<res['data'].length;i++){
  			this.sales.push(res['data'][i]);
  		}
  	});
  }

  getSalesDealer(group_id){
    this.salesdealer = [];
  	this.service.getAllSales(group_id).then(res=>{
  		for(let i=0;i<res['data'].length;i++){
  			this.salesdealer.push(res['data'][i]);
        this.salesdealerfull.push(res['data'][i]);
  		}
  	});
  }

  getSalesDealerPatria(parent_id){
    this.service.getAllSalesDealer(parent_id).then(res=>{
      for(let i=0;i<res['data'].length;i++){
        this.salesdealer.push(res['data'][i]);
        this.salesdealerfilter.push(res['data'][i]);
      }
    });
  }

  sorting() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Filter');

    alert.addInput({
      type: 'checkbox',
      label: 'Belum Verifikasi',
      value: '0',
      checked: this.filter[0]
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Terverifikasi',
      value: '1',
      checked: this.filter[1]
    });

    alert.addButton('Batal');
    alert.addButton({
      text: 'Filter',
      handler: data => {
        
        this.filter[0] = false;
        this.filter[1] = false;

        for(let i=0;i<data.length;i++){
          this.filter[data[i]] = true;
        }

        this.salesdealer = this.salesdealerfull;
        this.salesdealer = this.salesdealer.filter( function(item){
          if(data.length==2){
            return (item.active==0||item.active==1);
          }else{
            return (item.active==data[0]);
          }
        });
      }
    });
    alert.present();
  }

  detail(user_id){
  	this.navCtrl.push('SalesDetailPage', {user_id:user_id});
  }

  search_sales(nameKey, myArray){
    let new_list_dealer = [];
    for (var i=0; i < myArray.length; i++) {
      if(myArray[i]['full_name'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1 ) {
        new_list_dealer.push(myArray[i]);
      }
    }

    this.salesdealer = new_list_dealer;
  }

}

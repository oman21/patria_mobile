import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { Events } from 'ionic-angular';

/**
 * Generated class for the InboxDetilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inbox-detil',
  templateUrl: 'inbox-detil.html',
})
export class InboxDetilPage {
	items:any = [];
  loader:any;

  constructor(
  	public navCtrl: NavController, 
    public navParams: NavParams,
    public storage:Storage,
    public service: ServiceProvider,
    public detectorRef: ChangeDetectorRef,
    public events: Events,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ){

    this.loader = this.loadingCtrl.create({
        content: 'Memproses...',
    });
    this.loader.present();
  	
  	this.service.updateInbox(this.navParams.get('inbox_id')).then(()=>{
  		this.service.getInboxDetil(this.navParams.get('inbox_id')).then((res) => {
				this.items = res['data'];
        this.loader.dismiss();
			});
  	});
  }

 	sales(user_id){
  	this.navCtrl.push('SalesDetailPage', {user_id:user_id});
  }

  detil(url, id){
  	if(url=="PemesananDetilPage"){
  		this.navCtrl.push("PemesananDetilPage", {order_id:id});
  	}else if(url=="RewardDetailPage"){
      this.navCtrl.push("RewardDetailPage", {order_id:id});
    }else if(url=="SalesDetailPage"){
      this.navCtrl.push("SalesDetailPage", {user_id:id});
    }else{
      this.navCtrl.push(url, {id:id});
    }
  }

}

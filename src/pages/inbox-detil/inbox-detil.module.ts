import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InboxDetilPage } from './inbox-detil';

@NgModule({
  declarations: [
    InboxDetilPage,
  ],
  imports: [
    IonicPageModule.forChild(InboxDetilPage),
  ],
})
export class InboxDetilPageModule {}

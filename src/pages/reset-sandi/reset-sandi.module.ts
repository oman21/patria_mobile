import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResetSandiPage } from './reset-sandi';

@NgModule({
  declarations: [
    ResetSandiPage,
  ],
  imports: [
    IonicPageModule.forChild(ResetSandiPage),
  ],
})
export class ResetSandiPageModule {}

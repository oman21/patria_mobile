import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
/**
 * Generated class for the ResetSandiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reset-sandi',
  templateUrl: 'reset-sandi.html',
})
export class ResetSandiPage {

	email:any;
	data_error:any;
	loading:any;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public alertCtrl: AlertController,
  	public loadingCtrl: LoadingController,
  	public service: ServiceProvider,
  ) {
  	this.email = "";
  	this.data_error = "";
  }

  submit(){
  	if(this.email==""){
  		this.data_error = "Harus diisi!";
  	}else{
  		this.data_error = "";

  		this.loading = this.loadingCtrl.create({
          content: 'Memproses...',
      });
      this.loading.present();

      this.service.forgotPassword(this.email).then(res=>{
      	this.loading.dismiss();
      	if(res['status']=='200'){
      		if(res['data']['status']==false){
      			this.data_error = res['data']['message'];
      			let alert = this.alertCtrl.create({
			        title: res['data']['message'],
			        buttons: ['Ok']
			      });
			      alert.present();
    			}else{
    				let alert = this.alertCtrl.create({
			        title: res['data']['message'],
			        buttons: [
				        {
					        text: 'Ok',
					        handler: () => {
					        	this.navCtrl.pop();
					        }
					      }
			        ]
			      });
			      alert.present();
    			}
    		}else{
    			let alert = this.alertCtrl.create({
		        title: 'Err! Terjadi Kesalahan',
		        subTitle: 'Silahkan coba beberapa saat lagi',
		        buttons: ['Ok']
		      });
		      alert.present();	
    		}
      })
  	}
  }

}

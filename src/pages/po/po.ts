import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ActionSheetController  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { Events } from 'ionic-angular';

import {FormControl} from '@angular/forms';
import {Observable}  from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/throttleTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
/**
 * Generated class for the PoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-po',
  templateUrl: 'po.html',
})
export class PoPage {

	items:any = [];
	itemsTmp:any = [];
	datauser:any = [];
	loader:any;
	varian:any;
	kapasitas:any;
	data_error:any = [];

  estimasi_harga:any;
  reward_estimasi_harga:any;

  estimasiTermControl = new FormControl();
  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public storage:Storage, 
  	public service: ServiceProvider,
  	public alertCtrl: AlertController,
  	public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public events: Events
  ) {

  	this.storage.get('user_data').then((val) 	=> {
  		this.datauser.push(val);
  	});
  }

  ionViewDidLoad(){
    this.getHistori();
  }

  ionViewWillEnter() {
    this.loader = this.loadingCtrl.create({
        content: 'Memproses...',
    });
    this.loader.present();
    this.getHistori();
  }

  getHistori(){

    let query = 't_order.id='+this.navParams.get('id');
    this.service.getAllOrder(query).then(res=>{
      console.log('items',res['data']['data'][0]);
      this.items = res['data']['data'][0];
      this.itemsTmp.push(res['data']['data'][0]);
      this.loader.dismiss();
      this.varian = this.items.data_atribut_value[0].value_atribut;
      this.kapasitas = this.items.data_atribut_value[1].value_atribut;
      this.estimasi_harga = this.format(this.items['estimasi_harga']);
      console.log(this.items);
    });
  }

  format(valString) {

    console.log('this.itemsTmp[0]', this.itemsTmp[0]);

    if (!valString) {
        return '';
    }
      let val = valString.toString();
      const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);

      var new_format = parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR) + (!parts[1] ? '' : this.DECIMAL_SEPARATOR + parts[1]);
      var new_unformat = parseInt(this.unFormat(new_format));

      if(new_unformat == null || new_unformat == 0){
        this.estimasi_harga = this.itemsTmp[0]['total_harga'];
        this.data_error.estimasi_harga = "";
      }else{
        this.estimasi_harga = new_unformat;

        console.log('this.estimasi_harga', this.estimasi_harga);
        console.log('total harga', this.itemsTmp[0]['total_harga']);

        if(this.estimasi_harga < this.itemsTmp[0]['total_harga']){
          this.data_error.estimasi_harga = "Harus lebih besar dari haga normal!";
        }else{
          this.data_error.estimasi_harga = "";
          this.estimasi_harga = new_unformat;
          
          var harga_dasar_tmp = this.itemsTmp[0]['total_harga'];
          var reward_tmp = parseInt(this.itemsTmp[0]['total_reward']);
          var estimasi_harga_tmp = this.estimasi_harga;

          var reward_estimasi_harga_tmp = (estimasi_harga_tmp - harga_dasar_tmp)+reward_tmp;
          var reward_bulatan_harga = -Math.round(-(reward_estimasi_harga_tmp/1000000));

          console.log('harga_dasar_tmp', harga_dasar_tmp);
          console.log('reward_tmp', reward_tmp);
          console.log('estimasi_harga_tmp', estimasi_harga_tmp);

          console.log('reward_estimasi_harga_tmp', reward_estimasi_harga_tmp);
          console.log('reward_bulatan_harga', reward_bulatan_harga);

          if(reward_bulatan_harga > 1 && reward_bulatan_harga <= (5*this.itemsTmp[0]['quantity'])){
            this.reward_estimasi_harga = reward_bulatan_harga*1000000;
          }else if(reward_bulatan_harga > (5*this.itemsTmp[0]['quantity'])){
            this.reward_estimasi_harga = 5000000*this.itemsTmp[0]['quantity'];
          }else{
            this.reward_estimasi_harga = this.itemsTmp[0]['total_reward'];
          }

        }
      }

      return new_format;
  };

  unFormat(val) {
      if (!val) {
          return '';
      }
      val = val.replace(/^0+/, '');

      if (this.GROUP_SEPARATOR === ',') {
          return val.replace(/,/g, '');
      } else {
          return val.replace(/\./g, '');
      }
  };

  po(id){
  	if(this.data_error.estimasi_harga != ""){
			 return false;
    }

    let alert = this.alertCtrl.create({
      title: 'Konfirmasi PO',
      message: 'Apakah data PO yang anda kirim sudah benar?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Kirim',
          handler: () => {
            this.loader = this.loadingCtrl.create({
                content: 'Memproses...',
            });
            this.loader.present();

            this.po_process(id);
          }
        }
      ]
    });

    alert.present();
  }

  po_process(id){
  	let data = {
      id: id,
      status_order: "po_process",
      estimasi_harga: parseInt(this.unFormat(this.estimasi_harga)),
      estimasi_reward: this.reward_estimasi_harga
    }

    this.service.confirmOrder(data).then(res=>{
      let dataHistori = {
        id_order: id,
        action_from: this.navParams.get('status_before'),
        action_to: 'po_process',
        keterangan: 'Pengubahan status PO Terkirim',
        user_id: this.datauser[0]['user_id']
      }
      this.service.orderHistori(dataHistori).then(reshistori=>{
      
        let query = 't_order.id='+this.navParams.get('order_id');
        this.service.getAllOrder(query).then(res=>{

          let dataInboxPatria = {
            id_relation: id,
            tipe_relation: "PemesananDetilPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: this.datauser[0]['parent_id'],
            judul: "PO Terkirim",
            content: "Segera Process order dari sales anda. (PO Received)",
          }

          this.service.createInbox(dataInboxPatria).then(res_inbox=>{
            let pushPatria = {
              data:{
                message: "PO Terkirim",
                  tipe: "order_po_terkirim",
                  page: "InboxDetilPage",
                  parram: {
                    inbox_id:res_inbox['data']['id']
                  }
              }
            }

            this.service.pushNotifikasi(this.datauser[0]['parent_id'], pushPatria);
          });
          
          this.service.getManager().then(resMgr=>{
            for(var i=0;i<resMgr['data'].length;i++){

              var idManager = resMgr['data'][i]['user_id'];

              let dataInboxMgr = {
                id_relation: id,
                tipe_relation: "PemesananDetilPage",
                user_id_from: this.datauser[0]['user_id'],
                user_id_to: idManager,
                judul: "PO Received - Belum di proses",
                content: "PO Received - Belum di proses oleh sales patria",
              }

              this.sendpushmgr(idManager, dataInboxMgr);
            }
          });
        });
        
        this.loader.dismiss();
        this.getHistori();

        let alert = this.alertCtrl.create({
          title: 'Berhasil mengubah status ke PO Terkirim',
          subTitle: 'Order anda akan segera kami proses',
          buttons: [
            {
		          text: 'Tutup',
		          handler: () => {
		            this.navCtrl.pop();
		          }
		        }
          ]
        });
        alert.present();
      });
    })
  }

  sendpushmgr(idManager, dataInboxMgr){
    this.service.createInbox(dataInboxMgr).then(res_inbox=>{
      let pushMgr = {
        data:{
          message: "PO Received - Belum di proses",
            tipe: "order_confirm",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
        }
      }

      this.service.pushNotifikasi(idManager, pushMgr);
    });
  }

  total(request, estimasi){
    return parseInt(request)+parseInt(estimasi);
  }
}

import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { Events } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-inbox',
  templateUrl: 'inbox.html',
})
export class InboxPage {
	items:any = [];
	loader:any;
  constructor(
  	public navCtrl: NavController, 
    public navParams: NavParams,
    public storage:Storage,
    public service: ServiceProvider,
    public detectorRef: ChangeDetectorRef,
    public events: Events,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ){
  	this.loader = this.loadingCtrl.create({
        content: 'Memproses...',
    });
    this.loader.present();
    
  	this.getData();
  }

  ionViewWillEnter() {
  	this.getData();
  }

  getData(){
  	this.storage.get('user_data').then((val) => {
	  	this.service.getInbox(val.user_id).then((res) => {
	  		if(res['status']=='500'){
	  			this.loader.dismiss();
  			}else{
  				this.items = res['data'];
	  			this.loader.dismiss();
  			}
	  	});
		});
  }

  open(id){
  	this.navCtrl.push("InboxDetilPage", {inbox_id:id});
  }

  getInitial(string){
  	return string.slice(0,1);
  }
}

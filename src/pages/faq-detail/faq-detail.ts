import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the FaqDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faq-detail',
  templateUrl: 'faq-detail.html',
})
export class FaqDetailPage {


	title:any;
	content:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, protected sanitizer: DomSanitizer) {
  }

  ionViewDidLoad() {
  	this.title = this.navParams.get("data")['title'];
  	this.content = this.sanitizer.bypassSecurityTrustHtml(this.navParams.get("data")['content']); 
    console.log(this.navParams.get("data"));
  }

}
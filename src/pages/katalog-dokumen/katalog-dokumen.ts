import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';

import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-katalog-dokumen',
  templateUrl: 'katalog-dokumen.html',
})

export class KatalogDokumenPage{
	pdf:any;
	urlMaterial:any;
  katalog_name:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public service:ServiceProvider, private inAppBrowser:InAppBrowser) {
  	this.katalog_name = this.navParams.get('katalog_name');
    this.openWebPage();
  }

  openWebPage(){
    this.service.urlMaterial().then(url=>{
      const options: InAppBrowserOptions = {
        zoom: 'no'
      }
      const browser = this.inAppBrowser.create(url+'katalog/file_pdf/'+this.navParams.get('pdf'), '_self', options);

      console.log(browser);
    });
  } 

}

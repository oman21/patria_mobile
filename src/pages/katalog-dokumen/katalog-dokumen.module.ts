import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KatalogDokumenPage } from './katalog-dokumen';

@NgModule({
  declarations: [
    KatalogDokumenPage,
  ],
  imports: [
    IonicPageModule.forChild(KatalogDokumenPage),
  ],
})
export class KatalogDokumenPageModule {}

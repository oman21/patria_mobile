import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';

import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

import {FormControl} from '@angular/forms';
import {Observable}  from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/throttleTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';

/**
 * Generated class for the PurchasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-purchase',
  templateUrl: 'purchase.html',
})
export class PurchasePage {
	
  varian:any;
	kapasitas:any;
  attribut:any = [];
  urlMaterial:any;

  valueAttr:any = [];
  katalog_name:any;

  selecOption:any = [];
  input_jumlah:any;

  datauser:any=[];
  nama_customer:any;
  nama_proyek:any;

  groupid:any;

  loader:any;
  
  data_error:any = [];

  estimasi_harga:any;
  reward_estimasi_harga:any;
  jumlahTermControl = new FormControl();
  estimasiTermControl = new FormControl();
  DECIMAL_SEPARATOR=".";
  GROUP_SEPARATOR=",";

  request_name:any;
  request_check:any = false;
  request_value:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public service:ServiceProvider, 
    public storage:Storage,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private document: DocumentViewer,
    private inAppBrowser:InAppBrowser
  ) {
    this.data_error.jumlah = '';
    this.data_error.proyek = '';
    this.data_error.nama = '';
    this.data_error.estimasi_harga = '';
    this.data_error.request_value = '';

  	this.varian = "diesel";
  	this.kapasitas = 1;
    this.katalog_name = this.navParams.get('katalog_name');
    this.request_name = "";
    this.request_value = "";

    this.getAttribut(this.navParams.get('id'));

    this.service.urlMaterial().then(url=>{
      this.urlMaterial = url;
    });

    this.input_jumlah = 1;

    this.storage.get('user_data').then((val) => {
      this.groupid = val.group.id;
      this.datauser.push(val);
    });

    
  }

  format(valString) {
    if (!valString) {
        return '';
    }
      let val = valString.toString();
      const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);

      var new_format = parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR) + (!parts[1] ? '' : this.DECIMAL_SEPARATOR + parts[1]);
      var new_unformat = parseInt(this.unFormat(new_format));

      if(new_unformat == null || new_unformat == 0){
        this.estimasi_harga = this.valueAttr.harga_dasar*this.input_jumlah;
        this.data_error.estimasi_harga = "";
      }else{
        this.estimasi_harga = new_unformat;
        if(this.estimasi_harga < this.valueAttr.harga_dasar*this.input_jumlah){
          this.data_error.estimasi_harga = "Harus lebih besar dari haga normal!";
        }else{
          this.data_error.estimasi_harga = "";
          this.estimasi_harga = new_unformat;
          
          var harga_dasar_tmp = this.valueAttr.harga_dasar*this.input_jumlah;
          var reward_tmp = this.valueAttr.poin_dasar*this.input_jumlah;
          var estimasi_harga_tmp = this.estimasi_harga;

          var reward_estimasi_harga_tmp = (estimasi_harga_tmp - harga_dasar_tmp)+reward_tmp;
          var reward_bulatan_harga = -Math.round(-(reward_estimasi_harga_tmp/1000000));

          console.log('harga_dasar_tmp', harga_dasar_tmp);
          console.log('reward_tmp', reward_tmp);
          console.log('estimasi_harga_tmp', estimasi_harga_tmp);

          console.log('reward_estimasi_harga_tmp', reward_estimasi_harga_tmp);
          console.log('reward_bulatan_harga', reward_bulatan_harga);
          
          if(reward_bulatan_harga > 1 && reward_bulatan_harga <= (5*this.input_jumlah)){
            this.reward_estimasi_harga = reward_bulatan_harga*1000000;
          }else if(reward_bulatan_harga > (5*this.input_jumlah)){
            this.reward_estimasi_harga = 5000000*this.input_jumlah;
          }else{
            this.reward_estimasi_harga = this.valueAttr.poin_dasar*this.input_jumlah;
          }

        }
      }

      return new_format;
  };

  unFormat(val) {
      if (!val) {
          return '';
      }
      val = val.replace(/^0+/, '');

      if (this.GROUP_SEPARATOR === ',') {
          return val.replace(/,/g, '');
      } else {
          return val.replace(/\./g, '');
      }
  };



  ngOnInit(){
    // debounce
    this.jumlahTermControl.valueChanges.debounceTime(700).subscribe(newValue => {
      this.estimasi_harga = this.format(this.valueAttr.harga_dasar*newValue);
    })
  }

  dokumen(pdf){
    this.service.urlMaterial().then(url=>{

      this.inAppBrowser.create(url+'katalog/file_pdf/'+pdf, '_system');
    });
  }

  getAttribut(id){
    this.service.getProdukAttribut(id).then(res=>{

      for(let i=0;i<res['data'].length;i++){
        if(res['data'][i]['grup_atribut_value']=='REQUEST'){
          this.request_name = res['data'][i]['atribut'];
        }else{
          this.attribut.push(res['data'][i]);
        }
      }

      this.selecOption.push(res['data'][0].value);
    });
  }

  change(ev, n){
    if(ev==''){
      return false;
    }
    
    let pushAttr = [];
    for(let i=0;i<this.attribut.length;i++){
      for(let j=0;j<this.attribut[i].value.length;j++){
        pushAttr.push(this.attribut[i].value[j]);
      }
      
    }

    if((n+1)!=this.attribut.length){
      let newSelect = pushAttr.filter(item => item.kode_parent==ev);
      if(typeof this.selecOption[n+1] != 'undefined'){
        this.selecOption.splice(n+1);
      }

      this.selecOption.push(newSelect);

    }else{
      this.valueAttr = pushAttr.filter(item => item.id==ev)[0];
    }

    this.estimasi_harga = this.format(this.valueAttr.harga_dasar*this.input_jumlah);
    this.reward_estimasi_harga = this.valueAttr.poin_dasar*this.input_jumlah;
  }

  order(){
    if(
      this.input_jumlah < 1 ||
      (this.nama_customer=='' || this.nama_customer==null) ||
      (this.nama_proyek=='' || this.nama_proyek==null || this.data_error.estimasi_harga != "") ||
      (this.request_check==true && this.request_value=='')
    ){
      if(this.input_jumlah < 1){
        this.data_error.jumlah = 'Jumlah minimal 1';
      }else{
        this.data_error.jumlah = '';
      }

      if(this.nama_customer=='' || this.nama_customer==null){
        this.data_error.nama = 'Customer tidak boleh kosong!';
      }else{
        this.data_error.nama = '';
      }

      if(this.nama_proyek=='' || this.nama_proyek==null){
        this.data_error.proyek = 'Proyek tidak boleh kosong!';
      }else{
        this.data_error.proyek = '';
      }

      if(this.request_check==true && this.request_value==''){
        this.data_error.request_value = 'Note harus diisi';
      }else{
        this.data_error.request_value = '';
      }

      return false;
    }

    this.data_error.jumlah = '';
    this.data_error.proyek = '';
    this.data_error.nama = '';
    this.data_error.request_value = '';

    let alert = this.alertCtrl.create({
      title: 'Konfirmasi Order',
      message: 'Apakah data order yang anda kirim sudah benar?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Order',
          handler: () => {
            this.loader = this.loadingCtrl.create({
                content: 'Memproses...',
            });
            this.loader.present();

            this.order_process();
          }
        }
      ]
    });

    alert.present();
  }

  order_process(){
    let data = {
      id_produk: this.navParams.get('id'),
      id_atribut_value:  this.valueAttr.id,
      user_id: this.datauser[0]['user_id'],
      quantity: this.input_jumlah,
      jumlah_harga: this.valueAttr.harga_dasar,
      jumlah_reward: this.valueAttr.poin_dasar,
      nama_customer: this.nama_customer,
      nama_proyek :this.nama_proyek,
      estimasi_harga: parseInt(this.unFormat(this.estimasi_harga)),
      estimasi_reward: this.reward_estimasi_harga,
      request_note: this.request_value
    };

    this.service.order(data).then(res=>{

      let dataHistoi = {
        id_order: res['data'],
        action_from: '',
        action_to: 'waiting',
        keterangan: this.datauser[0]['full_name']+' melakukan order baru',
        user_id: this.datauser[0]['user_id']
      }
      
      this.service.orderHistori(dataHistoi).then(reshistori=>{

        let dataInboxPatria = {
          id_relation: res['data'],
          tipe_relation: "PemesananDetilPage",
          user_id_from: this.datauser[0]['user_id'],
          user_id_to: this.datauser[0]['parent_id'],
          judul: "Order Baru",
          content: "Anda mendapatkan order baru, silahkan diverifikasi",
        }

        this.service.createInbox(dataInboxPatria).then(res_inbox=>{
          let pushPatria = {
            data:{
              message: "Order Baru",
                tipe: "new_order",
                page: "InboxDetilPage",
                parram: {
                  inbox_id:res_inbox['data']['id']
                }
            }
          }

          this.service.pushNotifikasi(this.datauser[0]['parent_id'], pushPatria);
        });

        this.service.getManager().then(resMgr=>{
          for(var i=0;i<resMgr['data'].length;i++){
            
            var idManager = resMgr['data'][i]['user_id'];

            let dataInboxMgr = {
              id_relation: res['data'],
              tipe_relation: "PemesananDetilPage",
              user_id_from: this.datauser[0]['user_id'],
              user_id_to: idManager,
              judul: "Order Baru",
              content: "Ada order baru untuk sales anda",
            }

            this.sendpushmgr(idManager, dataInboxMgr);
          }
        });
        
      
        let alert = this.alertCtrl.create({
          title: 'Order anda telah berhasil dibuat',
          subTitle: 'sales kami akan segera memverifikasi data anda',
          buttons: [
            {
              text: 'Tutup',
              handler: () => {
                this.loader.dismiss();
                this.navCtrl.pop();
              }
            }
          ]
        });
        alert.present();
      });
    });
  }

  sendpushmgr(idManager, dataInboxMgr){
    this.service.createInbox(dataInboxMgr).then(res_inbox=>{
      let pushMgr = {
        data:{
          message: "Order Baru",
            tipe: "new_order",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
        }
      }

      this.service.pushNotifikasi(idManager, pushMgr);
    });
  }

  show_request(){
    console.log('request_check', this.request_check);
  }
}

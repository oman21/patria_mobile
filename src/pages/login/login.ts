import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { DatabaseProvider } from '../../providers/database/database';

import { HomePage } from '../home/home';
import { Network } from '@ionic-native/network';

import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device';

import { AuthStatePage } from '../auth-state/auth-state';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  password:any;
  email:any;
  password_type:any;
  eye:any;
  eye_status: any;
  resp_error:any;
  loading:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public network:Network,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public service: ServiceProvider,
    public database: DatabaseProvider,
    public storage:Storage,
    public device:Device
  ) {
    this.password = '';
    this.email = '';
    this.password_type = 'password';

    this.eye = 'eye';
    this.eye_status = false;

    this.resp_error = '';
  }

  eye_change(){
    if(this.eye_status==false){
      this.eye = 'eye-off';
      this.eye_status = true;
      this.password_type = 'text';
    }else{
      this.eye = 'eye';
      this.eye_status = false;
      this.password_type = 'password';
    }
  }

  reset(){
  	this.navCtrl.push("ResetSandiPage");
  }

  login(){
    if(this.network.type=="none"){
        let alert = this.alertCtrl.create({
          title: 'Koneksi Gagal!',
          subTitle: 'Pastikan perangkat anda terhubung dengan jaringan internet',
          buttons: ['OK']
        });
        alert.present();
      }else{
        if(this.email=='' || this.password==''){
          this.resp_error = 'Email/Password harus diisi!';
        }else{

          this.loading = this.loadingCtrl.create({
              content: 'Memproses...',
          });
          this.loading.present();
          let data = {
            email: this.email,
            password: this.password
          }
          this.service.loginProcess(data)
          .then(result => {
            if(result['status']==200){
              this.loading.dismiss();
              this.database.insertUser(result['data']);
              this.storage.get('fcm_token').then((val) => {
                let dataDevice = {
                  device_token: val,
                  user_id: result['data']['user_id'],
                  group_id: result['data']['group']['id'],
                  manufacturer:  this.device.manufacturer,
                  cordova:  this.device.cordova,
                  model:  this.device.model,
                  platform:  this.device.platform,
                  uuid: this.device.uuid,
                  version: this.device.version,
                }

                this.service.fcmRegister(dataDevice);
              });
              this.navCtrl.setRoot(HomePage);
            }else{
              this.loading.dismiss();
              this.resp_error = 'Username/Password Salah';
            }
          });
        }
      }
  }

  back(){
    this.navCtrl.setRoot(AuthStatePage);
  }
}

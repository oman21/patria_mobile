import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { LoginPage } from '../login/login';
 
/**
 * Generated class for the AuthStatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-auth-state',
  templateUrl: 'auth-state.html',
})
export class AuthStatePage {

	slider:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.slider = ['1.jpg', '2.jpg', '3.jpg'];
  }

  masuk(){
  	this.navCtrl.setRoot(LoginPage);
  }

  daftar(){
    this.navCtrl.push("DaftarPage");
  }

  terms(){
   this.navCtrl.push("TermsPage"); 
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuthStatePage } from './auth-state';

@NgModule({
  declarations: [
    AuthStatePage,
  ],
  imports: [
    IonicPageModule.forChild(AuthStatePage),
  ],
})
export class AuthStatePageModule {}

import { Component, ViewChild } from '@angular/core';
import { Nav, IonicPage, NavController, NavParams, AlertController, LoadingController, ActionSheetController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { DatabaseProvider } from '../../providers/database/database';

import { Network } from '@ionic-native/network';

import { Device } from '@ionic-native/device';
import { Storage } from '@ionic/storage';

import { AuthStatePage } from '../auth-state/auth-state';

import {SignaturePad} from 'angular2-signaturepad/signature-pad';

@IonicPage()
@Component({
  selector: 'page-daftar',
  templateUrl: 'daftar.html',
})
export class DaftarPage {
  
	dataForm:any = [];
	data_error:any = [];
	loading:any;

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  private signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 2,
    'canvasHeight': 200
  };

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public network:Network,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public service: ServiceProvider,
    public database: DatabaseProvider,
    public device: Device,
    public storage:Storage,  
    public nav: Nav
  ){
  	this.dataForm.nama="";
  	this.dataForm.email="";
  	this.dataForm.password="";
  	this.dataForm.password2="";
  	this.dataForm.hp="";
  	this.dataForm.bank="";
  	this.dataForm.rek="";
  	this.dataForm.rek_nama="";
    this.dataForm.nik="";
    this.dataForm.no_npwp="";
  	this.dataForm.cek=false;
    this.dataForm.cek_sign=false;
    this.dataForm.signature="";
    this.dataForm.alamat_dealer = "";
    this.dataForm.nama_bc="";
  }

  ngAfterViewInit() {
    let canvas = document.querySelector('canvas');
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.set('canvasWidth', canvas.offsetWidth);
    this.signaturePad.set('canvasHeight', canvas.offsetHeight);
    this.signaturePad.clear();
  }

  drawComplete() {
    console.log("signature", this.signaturePad.toDataURL());
    this.dataForm.signature = this.signaturePad.toDataURL();
  }

  drawClear() {
    this.signaturePad.clear();
    this.dataForm.signature="";
  }

  skb_open(){
    if(
      this.dataForm.nama=="" || 
      this.dataForm.bank=="" ||
      this.dataForm.nik=="" ||
      this.dataForm.rek=="" ||
      this.dataForm.rek_nama=="" ||
      this.dataForm.signature=="" ||
      this.dataForm.alamat_dealer=="" ||
      this.dataForm.cek==false
    ){
      if(this.dataForm.nama==""){
          this.data_error.nama = "Nama harus diisi!";
        }else{
          this.data_error.nama = "";
        }

        if(this.dataForm.nik==""){
          this.data_error.nik = "NIK harus diisi!";
        }else{
          this.data_error.nik = "";
        }

        if(this.dataForm.bank==""){
          this.data_error.bank = "Nama Bank harus diisi!";
        }else{
          this.data_error.bank = "";
        }

        if(this.dataForm.rek==""){
          this.data_error.rek = "No.Rekening harus diisi!";
        }else{
          this.data_error.rek = "";
        }

        if(this.dataForm.rek_nama==""){
          this.data_error.rek_nama = "Rekening Atas Nama harus diisi!";
        }else{
          this.data_error.rek_nama = "";
        }

        if(this.dataForm.alamat_dealer==""){
          this.data_error.alamat_dealer = "Alamat Dealer harus diisi!";
        }else{
          this.data_error.alamat_dealer = "";
        }

        if(this.dataForm.cek==false){
          this.data_error.cek = "Anda harus menyetujui ini";  
        }else{
          this.data_error.cek = ""; 
        }

        if(this.dataForm.signature==""){
          this.data_error.signature = "Anda harus menandatangani ini";  
        }else{
          this.data_error.signature = ""; 
        }

      let alert = this.alertCtrl.create({
        title: 'Surat Keputusan Bersama',
        subTitle: 'Pastikan data anda sudah lengkap',
        buttons: ['OK']
      });
      alert.present();
    }else{
      this.data_error.nama = "";
      this.data_error.signature = ""; 
      this.data_error.cek = ""; 
      this.data_error.alamat_dealer = "";
      this.data_error.rek_nama = "";
      this.data_error.rek = "";
      this.data_error.bank = "";
      this.data_error.nik = "";

      this.navCtrl.push("SuratKeputusanBersamaPage", {data:this.dataForm});
    }
  }

  daftar(){
  	if(this.network.type=="none"){
      let alert = this.alertCtrl.create({
        title: 'Koneksi Gagal!',
        subTitle: 'Pastikan perangkat anda terhubung dengan jaringan internet',
        buttons: ['OK']
      });
      alert.present();
    }else{
    	this.loading = this.loadingCtrl.create({
          content: 'Memproses...',
      });
      this.loading.present();
	  	if(
	  		this.dataForm.nama=="" || 
	  		this.dataForm.email=="" ||
	  		this.dataForm.password=="" ||
		  	this.dataForm.password2=="" ||
		  	this.dataForm.hp=="" ||
		  	this.dataForm.bank=="" ||
        this.dataForm.nik=="" ||
		  	this.dataForm.rek=="" ||
		  	this.dataForm.rek_nama=="" ||
        this.dataForm.no_npwp=="" ||
		  	this.dataForm.cek==false ||
        this.dataForm.cek_sign==false ||
        this.dataForm.signature=="" ||
        this.dataForm.nama_bc==""
	  	){
	  		if(this.dataForm.nama==""){
	  			this.data_error.nama = "Nama harus diisi!";
	  		}else{
	  			this.data_error.nama = "";
	  		}

        if(this.dataForm.no_npwp==""){
          this.data_error.no_npwp = "NPWP harus diisi!";
        }else{
          this.data_error.no_npwp = "";
        }

	  		if(this.dataForm.email==""){
	  			this.data_error.email = "Email harus diisi!";
	  		}else{
	  			this.data_error.email = "";
	  		}

	  		if(this.dataForm.password==""){
	  			this.data_error.password = "password harus diisi!";
	  		}else{
	  			this.data_error.password = "";
	  		}

	  		if(this.dataForm.password2==""){
	  			this.data_error.password2 = "Konfirmasi Password harus diisi!";
	  		}else{
	  			this.data_error.password2 = "";
	  		}

        if(this.dataForm.nik==""){
          this.data_error.nik = "NIK harus diisi!";
        }else{
          this.data_error.nik = "";
        }

	  		if(this.dataForm.hp==""){
	  			this.data_error.hp = "No.Handphone harus diisi!";
	  		}else{
	  			this.data_error.hp = "";
	  		}

	  		if(this.dataForm.bank==""){
	  			this.data_error.bank = "Nama Bank harus diisi!";
	  		}else{
	  			this.data_error.bank = "";
	  		}

	  		if(this.dataForm.rek==""){
	  			this.data_error.rek = "No.Rekening harus diisi!";
	  		}else{
	  			this.data_error.rek = "";
        }
        
        if(this.dataForm.nama_bc==""){
	  			this.data_error.nama_bc = "Nama Business Consultan harus diisi!";
	  		}else{
	  			this.data_error.nama_bc = "";
	  		}

	  		if(this.dataForm.rek_nama==""){
	  			this.data_error.rek_nama = "Rekening Atas Nama harus diisi!";
	  		}else{
	  			this.data_error.rek_nama = "";
	  		}

	  		if(this.dataForm.cek==false){
	  			this.data_error.cek = "Anda harus menyetujui ini";	
	  		}else{
	  			this.data_error.cek = "";	
	  		}

        if(this.dataForm.cek_sign==false){
          this.data_error.cek_sign = "Anda harus menyetujui ini";  
        }else{
          this.data_error.cek_sign = ""; 
        }

        if(this.dataForm.signature==""){
          this.data_error.signature = "Anda harus menandatangani ini";  
        }else{
          this.data_error.signature = ""; 
        }

	  		this.loading.dismiss();
	  	}else{

	  		if(this.dataForm.password!=this.dataForm.password2){
	  			this.data_error.password2 = "Password tidak sama!";
	  			this.loading.dismiss();
	  			return false;
	  		}else{
	  			this.data_error.password2 = "";
	  		}

	  		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				if(!re.test(this.dataForm.email)) {
				  this.data_error.email = "Email tidak sesuai!";
					this.loading.dismiss();
					return false;
				}else{
					this.data_error.email = "";
				}

  			this.service.daftarProcess(this.dataForm)
	        .then(result => {
	          if(result['status']==200){
	          	//this.database.insertUser(result['data']);
	          	this.storage.get('fcm_token').then((val) => {
					      let dataDevice = {
					      	device_token: val,
					      	user_id: result['data']['user_id'],
					      	group_id: result['data']['group']['id'],
					      	manufacturer:  this.device.manufacturer,
					      	cordova:  this.device.cordova,
					      	model:  this.device.model,
					      	platform:  this.device.platform,
					      	uuid: this.device.uuid,
					      	version: this.device.version
					      }

					      this.service.fcmRegister(dataDevice);
					    });

	          	this.service.getManager().then(resMgr=>{
                for(var i=0;i<resMgr['data'].length;i++){

                  var idManager = resMgr['data'][i]['user_id'];

                  console.log("id manager2", idManager);

                  let dataInboxPatria = {
  			            id_relation: result['data']['user_id'],
  			            tipe_relation: "SalesDetailPage",
  			            user_id_from: result['data']['user_id'],
  			            user_id_to: idManager,
  			            judul: "Pendaftaran Sales Baru",
  			            content: "Ada pendaftaran sales baru untuk di onfirmasi",
  			          }

                  this.sendpushmgr(idManager, dataInboxPatria);
                }
			        });

	            this.loading.dismiss();

	            let alert = this.alertCtrl.create({
					        title: 'Terima kasih telah mendaftar',
					        subTitle: 'Akun anda akan kami verifikasi',
					        buttons: [{
					          text: 'OK',
					          handler: () => {
					            this.nav.setRoot(AuthStatePage);
					          }
					        }]
					    });
					    alert.present();
			  			
			  			}else if(result['status']==300){
                this.loading.dismiss();
			  				let alert2 = this.alertCtrl.create({
					        title: 'Pendaftaran Gagal',
					        subTitle: result['data'],
					        buttons: ['OK']
				        });

				        alert2.present();
              }else{
                this.loading.dismiss();
              	let alert2 = this.alertCtrl.create({
					        title: 'Pendaftaran Gagal',
					        subTitle: 'Pastikan perangkat anda terhubung dengan internet',
					        buttons: ['OK']
				        });
              }
	        });
	  	}
	  }
  }

  sendpushmgr(idManager, dataInboxPatria){
    this.service.createInbox(dataInboxPatria).then(res_inbox=>{
      let pushMgr = {
        data:{
          message: "Pendaftaran Sales Baru",
            tipe: "new_sales",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
        }
      }

      console.log("id manager", idManager);
      console.log("data dikirim", pushMgr);
      

      this.service.pushNotifikasi(idManager, pushMgr);
    });
  }

  terms(){
  	this.navCtrl.push("TermsPage");
  }
}

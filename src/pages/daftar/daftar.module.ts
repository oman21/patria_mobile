import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DaftarPage } from './daftar';
import { SignaturePadModule } from 'angular2-signaturepad';


@NgModule({
  declarations: [
    DaftarPage,
  ],
  imports: [
    IonicPageModule.forChild(DaftarPage),
    SignaturePadModule
  ],
})
export class DaftarPageModule {}

import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, AlertController, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { Events } from 'ionic-angular';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
	slider:any = [];
  datauser:any = [];
  groupid:any;
  notifSales:any;
  notifInbox:any;
  rewardpoint:any;
  urlMaterial:any;

  allPatria:any = [];
  allDealer:any = [];
  allDealerChild:any = [];
  kantor:any = [];

  popUpUpdate:any;
  cek_update_profile:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public storage:Storage,
    public service: ServiceProvider,
    public detectorRef: ChangeDetectorRef,
    public events: Events,
    private toastCtrl: ToastController,
    private appVersion: AppVersion,
    public platform: Platform,
    public alertCtrl: AlertController,
    public app: App,
    public device: Device
  ) {

    this.initializeBackButtonCustomHandler();

    this.popUpUpdate = false;

    this.checkAppVersion();

    this.getPromo();
    this.service.urlMaterial().then(url=>{
      this.urlMaterial = url;
    });

    this.getNotifInbox();

    this.storage.get('user_data').then((val) => {
      this.service.getKantor().then(res_kantor=>{
        this.kantor = res_kantor['data'];
      });
      
      this.groupid = val.group.id;
      this.datauser = val;

      if(this.groupid==3){

        this.getNotifSalesBaru();
        
        /*this.events.subscribe('notif_sales_baru', language => {
          this.getNotifSalesBaru();

          this.detectorRef.detectChanges();
        });*/

        this.getAllPatria();
        this.getAllDealer();

      }else if(this.groupid==4){

        this.getAllDealerChild(val.user_id);

      }else if(this.groupid==5){
        this.getRewardPoint(this.datauser.user_id);
      }

      console.log("datauser", val);

      this.storage.get('fcm_token').then((fcm) => {
        let dataDevice = {
          device_token: fcm,
          user_id: val['user_id'],
          group_id: val['group']['id'],
          manufacturer:  this.device.manufacturer,
          cordova:  this.device.cordova,
          model:  this.device.model,
          platform:  this.device.platform,
          uuid: this.device.uuid,
          version: this.device.version,
        }

        this.service.fcmRegister(dataDevice);
      });
    });
  }

  checkAppVersion(){
    this.appVersion.getVersionNumber().then(version => {
      this.service.getAppVersion().then(res=>{
        if(version != res['data']){
          this.popUpUpdate = true;
          console.log("apk harus update");
        }else{
          this.popUpUpdate = false;
          console.log("apk tidak harus update");
        }
      });
    });
  }

  getAllPatria(){
    this.service.getAllUserPatria().then(res=>{
      this.allPatria = res['data'];
    });
  }

  getAllDealer(){
    this.service.getAllUserDealer().then(res=>{
      this.allDealer = res['data'];
    });
  }

  getAllDealerChild(user_id){
    this.service.getAllUserDealerChild(user_id).then(res=>{
      this.allDealerChild = res['data'];
    });
  }

  ionViewWillEnter() {
    this.cek_update_profile = false;
    this.getNotifInbox();
    this.cekProfile();
  }

  cekProfile(){
    this.storage.get('user_data').then((val) => {
      this.service.cekProfile(val.user_id).then(res=>{
        if(res['status']=='200'){
          if(res['data']=='update'){
            this.cek_update_profile = true;
          }else{
            this.cek_update_profile = false;
          }
        }
      });
    });
  }

  goTo(link){
    this.navCtrl.push(link);
  }

  getRewardPoint(user_id){
    this.service.getRewardPoint(user_id).then(res=>{
      if(res['data']['status']==true){
        this.rewardpoint = res['data']['saldo'];
      }else{
        this.rewardpoint = 0;
      }
    });
  }

  getPromo(){
    this.service.getPromo().then(res=>{
      this.slider = res['data'];
      console.log(this.slider);
    });
  }

  getNotifSalesBaru(){
    this.service.notifSalesBaru().then(res=>{
      this.notifSales = res['data'];
    });
  }

  getNotifInbox(){
    this.storage.get('user_data').then((val) => {
      this.service.getNotifInbox(val.user_id).then((res) => {
        this.notifInbox = res['data'].length;
      });
    });
  }

  katalog(){
  	this.navCtrl.push('KatalogPage');
  }

  pemesanan(){
  	this.navCtrl.push('PemesananPage');
  }

  sales(){
   this.navCtrl.push('SalesPage'); 
  }

  reward(){
    this.navCtrl.push('RewardPage');
  }

  inbox(){
    this.navCtrl.push('InboxPage');
  }

  rewardPatria(){
    this.navCtrl.push('RewardPatriaPage');
  }

  refreshReward(){
    this.storage.get('user_data').then((val) => {
      this.getRewardPoint(this.datauser.user_id);
    });
  }

  refresh(refresher){
    this.popUpUpdate = false;

    this.checkAppVersion();
    
    this.getPromo();
    this.service.urlMaterial().then(url=>{
      this.urlMaterial = url;
    });

    this.getNotifInbox();

    this.storage.get('user_data').then((val) => {
      this.groupid = val.group.id;
      this.datauser = val;

      if(this.groupid==3){

        this.getNotifSalesBaru();
        
        this.events.subscribe('notif_sales_baru', language => {
          this.getNotifSalesBaru();

          this.detectorRef.detectChanges();
        });

        this.getAllPatria();
        this.getAllDealer();

      }else if(this.groupid==4){

        this.getAllDealerChild(val.user_id);

      }else if(this.groupid==5){
        this.getRewardPoint(this.datauser.user_id);
      }
    });
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  about(){
    this.storage.get('user_data').then((val) => {
      this.navCtrl.push('SalesDetailPage', {user_id:val['parent_id']});
    });
  }

  goToPromo(data){
    this.navCtrl.push("PromoDetilPage", {id:data.id});
  }

  playstore(){
    window.open("market://details?id=com.ahza.patria", "_system");
  }

  initializeBackButtonCustomHandler(){
    this.platform.registerBackButtonAction(() => {
      if(this.navCtrl.getActive().name=="HomePage"){
        let confirm = this.alertCtrl.create({
          title: 'Keluar Aplikasi',
          message: 'Apakah anda mau keluar dari aplikasi Patria Mobile?',
          buttons: [
            {
              text: 'Tidak',
              handler: () => {
                
              }
            },
            {
              text: 'Ya',
              handler: () => {
                this.platform.exitApp();
              }
            }
          ]
        });
        confirm.present();
      }else{
        this.navCtrl.pop();
      }
    })
  }

}

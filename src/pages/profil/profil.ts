import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ActionSheetController  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { DatabaseProvider } from '../../providers/database/database';

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { Events } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-profil',
  templateUrl: 'profil.html',
})
export class ProfilPage {
	datauser:any = [];
	urlMaterial:any;
  groupid:any;
  imageURI:any;
  infouser:any =[];
  photoprofil:any;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public storage:Storage,
      public service: ServiceProvider,
      private transfer: FileTransfer,
      private camera: Camera,
      public loadingCtrl: LoadingController,
      public toastCtrl: ToastController,
      public database: DatabaseProvider,
      public events: Events,
      public actionSheetCtrl: ActionSheetController,
      private inAppBrowser:InAppBrowser
  ) {
  	this.service.urlMaterial().then(url=>{
  		this.urlMaterial = url;
  	});

  	this.storage.get('user_data').then((val) => {
      console.log('profil di storage',val);
      this.photoprofil = val['photo'];
      this.datauser = val;
      this.infouser.push(val);
    });
  }

  ionViewWillEnter() {
    this.storage.get('user_data').then((val) => {
      this.datauser = val;
      this.groupid = val.group.id;
      this.photoprofil = val['photo'];
      this.infouser.push(val);
    }); 
  }

  edit(){
    this.navCtrl.push("EditProfilPage");
  }

  ganti(){
    this.navCtrl.push("UbahSandiPage");
  }

  lihat_patria(parent_id){
    this.navCtrl.push('SalesDetailPage', {user_id:parent_id});
  }

  ganti_foto(){
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Pilih Mode',
      buttons: [
        {
          text: 'Kamera',
          handler: () => {
            this.ganti_foto_camera();
          }
        },{
          text: 'Gallery',
          handler: () => {
            this.ganti_foto_gallery();
          }
        },{
          text: 'Batal',
          role: 'cancel',
        }
      ]
    });
    actionSheet.present();
  }

  ganti_foto_gallery(){
    const options: CameraOptions = {
      quality: 20,
      targetWidth: 281,
      targetHeight: 281,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true //may not work with some deices
    }

    this.camera.getPicture(options).then((imageData) => {
      console.log('imageData',imageData);
      this.imageURI = "data:image/jpeg;base64," + imageData;
      this.uploadFile();
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }

  ganti_foto_camera(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      //sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 300,
      targetHeight: 300,
      saveToPhotoAlbum: true
    }

    this.camera.getPicture(options).then((imageData) => {
      console.log('imageData',imageData);
      this.imageURI = "data:image/jpeg;base64," + imageData;
      this.uploadFile();
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }

  uploadFile() {
    this.photoprofil = '';
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
    let photoTmp = this.infouser[0]['full_name'];
    let photoName = photoTmp.replace(/ /g,"_");
    let options = {
      fileKey: 'ionicfile',
      fileName: photoName+'.jpg',
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }

    fileTransfer.upload(this.imageURI, 'http://apps.patria.co.id/api/users/user_photo/id/'+this.infouser[0]['user_id'], options)
    .then((data) => {
      const result = JSON.parse(data['response']);
      let new_userdata = {
        activation_code: this.infouser[0]['activation_code'],
        active:this.infouser[0]['active'],
        alamat_dealer:this.infouser[0]['alamat_dealer'],
        bank:this.infouser[0]['bank'],
        created_on:this.infouser[0]['created_on'],
        dealer:this.infouser[0]['dealer'],
        email:this.infouser[0]['email'],
        first_name:this.infouser[0]['first_name'],
        forgotten_password_code:this.infouser[0]['forgotten_password_code'],
        forgotten_password_time:this.infouser[0]['forgotten_password_time'],
        full_name:this.infouser[0]['full_name'],
        group:{
          description:this.infouser[0]['group']['description'],
          id:this.infouser[0]['group']['id'],
          name:this.infouser[0]['group']['name']
        },
        id:this.infouser[0]['id'],
        ip_address:this.infouser[0]['ip_address'],
        last_login:this.infouser[0]['last_login'],
        last_name:this.infouser[0]['last_name'],
        no_rekening:this.infouser[0]['no_rekening'],
        rekening_atas_nama:this.infouser[0]['rekening_atas_nama'],
        parent_id:this.infouser[0]['parent_id'],
        password:this.infouser[0]['password'],
        phone:this.infouser[0]['phone'],
        photo:result['filename'],
        remember_code:this.infouser[0]['remember_code'],
        salt:this.infouser[0]['salt'],
        user_id:this.infouser[0]['user_id'],
        username:this.infouser[0]['username']
      }

      this.photoprofil = result['filename'];

      this.database.insertUser(new_userdata);
      loader.dismiss();
      this.presentToast("Image uploaded successfully");
    }, (err) => {
      console.log(err);
      loader.dismiss();
      this.presentToast(err);
    });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  skb(pdf){
    this.service.urlMaterial().then(url=>{
      var options:any = {
        location: 'no',
        clearcache: 'yes'
      };

      var timestamp = new Date().getTime();
      this.inAppBrowser.create(url+'surat_kesepakatan/'+pdf+'?'+timestamp, '_system', options);
    });
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { DomSanitizer } from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-surat-keputusan-bersama',
  templateUrl: 'surat-keputusan-bersama.html',
})
export class SuratKeputusanBersamaPage {
	data:any;

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	public service: ServiceProvider,
  	protected sanitizer: DomSanitizer
  ) {

  	var myData = this.navParams.get('data');
  	this.service.suratKesepakatanBersama(myData).then(res=>{
  		console.log("res", res);
  		this.data = this.sanitizer.bypassSecurityTrustHtml(res['data']);
  	});
  }
}

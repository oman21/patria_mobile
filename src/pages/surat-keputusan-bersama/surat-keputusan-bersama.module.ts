import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuratKeputusanBersamaPage } from './surat-keputusan-bersama';

@NgModule({
  declarations: [
    SuratKeputusanBersamaPage,
  ],
  imports: [
    IonicPageModule.forChild(SuratKeputusanBersamaPage),
  ],
})
export class SuratKeputusanBersamaPageModule {}

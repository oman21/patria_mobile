import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromoDetilPage } from './promo-detil';

@NgModule({
  declarations: [
    PromoDetilPage,
  ],
  imports: [
    IonicPageModule.forChild(PromoDetilPage),
  ],
})
export class PromoDetilPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';

/**
 * Generated class for the PromoDetilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-promo-detil',
  templateUrl: 'promo-detil.html',
})
export class PromoDetilPage {
	promo:any;
	urlMaterial:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public service: ServiceProvider) {
  	this.service.urlMaterial().then(url=>{
      this.urlMaterial = url;
    });
    this.service.getDetailPromo(this.navParams.get('id')).then((res) => {
      this.promo = res['data']['data'];
      console.log(this.promo);
    });
  }

}

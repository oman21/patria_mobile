import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-reward',
  templateUrl: 'reward.html',
})
export class RewardPage {
	items:any = [];
  itemsfilter:any = [];
  itemsall:any = [];
	collect:any = [];
	modal:any;
	tabs:any;
  myclaim:any = [];
  myclaimfilter:any = [];
  loader:any;
  searchKeyList:any;
  searchKeyMy:any;
  filter:any = [];

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public storage:Storage,
    public service: ServiceProvider,
    public detectorRef: ChangeDetectorRef,
    public events: Events,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController
  ){


    this.filter[1] = true;
    this.filter[2] = true;
    this.filter[3] = true;
    this.filter[4] = true;

    this.loader = this.loadingCtrl.create({
        content: 'Memproses...',
    });
    this.loader.present();

  	this.tabs = 'claim';
  	this.storage.get('user_data').then((val) 	=> {
  		let query = "t_order.status_order='finish' AND t_order.is_claim_reward!='1' AND t_order.user_id="+val.user_id;
  		
	  	this.service.getAllOrder(query).then(res=>{
	  		if(res['status']=='500'){
          this.loader.dismiss();
        }else{
          this.items = res['data']['data'];
          this.itemsfilter = res['data']['data'];
          for(let i=0;i<res['data']['data'].length;i++){
            this.loader.dismiss();
          }
        }
	  	});
  	

    	this.modal = 'option-claim-close';

      let query2 = "t_order.status_order='finish' AND t_order.is_claim_reward > '0' AND t_order.user_id="+val.user_id;
      
      this.service.getAllOrder(query2).then(res=>{
        if(res['status']=='500'){
            this.myclaim = [];
            this.myclaimfilter = [];
        }else{
          for(let i=0;i<res['data']['data'].length;i++){
            this.myclaim = res['data']['data'];
            this.myclaimfilter = res['data']['data'];
            this.itemsall.push(res['data']['data'][i]);
            this.loader.dismiss();
          }
        }
      });

    });
  }

  emptyCollect(){
    this.collect = [];
    this.modal = 'option-claim-close';
  }

  collectdata(ev, id){
  	if(ev.currentTarget.checked==true){
  		this.collect.push(id);
  	}else{
  		var index = this.collect.indexOf(id);
			if (index >= 0) {
			  this.collect.splice( index, 1 );
			}
  	}

  	if(this.collect.length > 0){
  		this.modal = 'option-claim-open';
  	}else{
  		this.modal = 'option-claim-close';
  	}
  }

  claim(){
  	this.navCtrl.push("RewardConfirmPage", {collect:this.collect});
  }

  rewardDetail(order_id){
    this.navCtrl.push("RewardDetailPage", {order_id:order_id});
  }

  sorting() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Filter');

    alert.addInput({
      type: 'checkbox',
      label: 'Waiting',
      value: '2',
      checked: this.filter[2]
    });

    alert.addInput({
      type: 'checkbox',
      label: 'On Process',
      value: '3',
      checked: this.filter[3]
    });

    /*alert.addInput({
      type: 'checkbox',
      label: 'Rejected',
      value: '4',
      checked: this.filter.rejected
    });*/

    alert.addInput({
      type: 'checkbox',
      label: 'Verified Release',
      value: '4',
      checked: this.filter[4]
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Finish',
      value: '1',
      checked: this.filter[1]
    });

    alert.addButton('Batal');
    alert.addButton({
      text: 'Filter',
      handler: data => {
        this.myclaim = [];

        this.filter[1] = false;
        this.filter[2] = false;
        this.filter[3] = false;
        this.filter[4] = false;

        for(let i=0;i<data.length;i++){
          this.filter[data[i]] = true;
          var newdata = this.itemsall.filter( function(item){
            return (item.is_claim_reward==data[i]);
          });

          if(newdata.length>0){
            for(let j=0;j<newdata.length;j++){
              this.myclaim.push(newdata[j]);
            }
          }
        }
        
      }
    });
    alert.present();
  }

  search_reward_list(nameKey, myArray){
    let new_data = [];
    for (var i=0; i < myArray.length; i++) {
      if(
        myArray[i]['nama_customer'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1 ||
        myArray[i]['nama_proyek'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1 ||
        myArray[i]['nama_produk'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1
        
      ) {
        new_data.push(myArray[i]);
      }
    }

    this.items = new_data;
  }

  search_reward_my(nameKey, myArray){
    let new_data = [];
    for (var i=0; i < myArray.length; i++) {
      if(
        myArray[i]['nama_customer'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1 ||
        myArray[i]['nama_proyek'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1 ||
        myArray[i]['nama_produk'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1
        
      ) {
        new_data.push(myArray[i]);
      }
    }

    this.myclaim = new_data;
  }

}

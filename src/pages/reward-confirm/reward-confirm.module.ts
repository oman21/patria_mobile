import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RewardConfirmPage } from './reward-confirm';

@NgModule({
  declarations: [
    RewardConfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(RewardConfirmPage),
  ],
})
export class RewardConfirmPageModule {}

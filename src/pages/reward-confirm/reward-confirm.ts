import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-reward-confirm',
  templateUrl: 'reward-confirm.html',
})
export class RewardConfirmPage {
	items:any = [];
	collect:any = [];
	total_reward:any;
	loader:any;
	datauser:any = [];

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public storage:Storage,
    public service: ServiceProvider,
    public detectorRef: ChangeDetectorRef,
    public events: Events,
    public alertCtrl: AlertController,
  	public loadingCtrl: LoadingController,
  ){

  	this.storage.get('user_data').then((val) 	=> {
  		this.datauser.push(val);
  	});

  	this.total_reward = 0;
  	let collect = this.navParams.get('collect');
  	for(let i=0;i<collect.length;i++){
  		let query = "t_order.id="+collect[i];
  		
	  	this.service.getAllOrder(query).then(res=>{
	  		this.items.push(res['data']['data'][0]);
	  		this.total_reward+=Number(res['data']['data'][0]['estimasi_reward']);
	  	});
  	}
  }

  ajukan(){
  	let confirm = this.alertCtrl.create({
      title: 'Ajukan Claim Reward',
      message: 'Apakah anda yakin mau mengajukan ini?',
      buttons: [
        {
          text: 'Batal',
          handler: () => {
            
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.loader = this.loadingCtrl.create({
                content: 'Memproses...',
            });
            this.loader.present();

            this.ajukan_process();  
          }
        }
      ]
    });
    confirm.present();
  }

  ajukan_process(){
  	for(let i=0;i<this.items.length;i++){
  		let data = {
  			user_id: this.datauser[0]['user_id'],
        id_order: this.items[i]['id'],
        status: 'waiting',
        keterangan: 'Melakukan claim rewards',
  		}

  		this.service.insertClaim(data).then(res=>{console.log('insert claim', res);});

  		let update = {
  			id:this.items[i]['id'],
  			is_claim_reward: 2
  		}

  		this.service.confirmOrder(update).then(res=>{console.log('update', res);});

      let dataInboxPatria = {
        id_relation: this.items[i]['id'],
        tipe_relation: "RewardDetailPage",
        user_id_from: this.datauser[0]['user_id'],
        user_id_to: this.datauser[0]['parent_id'],
        judul: "Pengajuan Claim Reward",
        content: "Sales anda telah mengajukan claim reward",
      }

      this.service.createInbox(dataInboxPatria).then(res_inbox=>{
        let pushPatria = {
          data:{
            message: "Pengajuan Claim Reward",
              tipe: "claim_reward",
              page: "InboxDetilPage",
              parram: {
                inbox_id:res_inbox['data']['id']
              }
          }
        }

        this.service.pushNotifikasi(this.datauser[0]['parent_id'], pushPatria);
      });

      this.service.getManager().then(resMgr=>{
        console.log('notif ke manager', resMgr);
        for(var j=0;j<resMgr['data'].length;j++){

          var idManager = resMgr['data'][j]['user_id'];

          let dataInboxMgr = {
            id_relation: this.items[i]['id'],
            tipe_relation: "RewardDetailPage",
            user_id_from: this.datauser[0]['user_id'],
            user_id_to: idManager,
            judul: "Pengajuan Claim Reward",
            content: "Salesman anda telah mengajukan claim reward ke Sales Patria BC",
          }

          this.sendpushmgr(idManager, dataInboxMgr);
        }
      });
  	}

  	this.loader.dismiss();
  	this.navCtrl.popTo( this.navCtrl.getByIndex(0));

    let alert = this.alertCtrl.create({
      title: 'Pengajuan Claim',
      subTitle: 'Pengajuan claim anda akan di proses',
      buttons: [
        {
          text: 'Tutup',
        }
      ]
    });
    alert.present();
  }

  sendpushmgr(idManager, dataInboxMgr){
    console.log("Pengajuan claim reward", idManager, dataInboxMgr);
    this.service.createInbox(dataInboxMgr).then(res_inbox=>{
      console.log("res_inbox", res_inbox);
      let pushMgr = {
        data:{
          message: "Pengajuan Claim Reward",
            tipe: "claim_reward",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
        }
      }

      this.service.pushNotifikasi(idManager, pushMgr);
    });
  }

}

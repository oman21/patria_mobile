import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { DatabaseProvider } from '../../providers/database/database';

import { HomePage } from '../home/home';
import { Network } from '@ionic-native/network';

import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-ubah-sandi',
  templateUrl: 'ubah-sandi.html',
})
export class UbahSandiPage {
	dataForm:any = [];
	err:any = [];
	userdata:any = [];
  loading:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public network:Network,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public service: ServiceProvider,
    public database: DatabaseProvider,
    public storage:Storage,  
  ) {
    this.dataForm.old_password='';
    this.dataForm.new_password='';
    this.dataForm.new_password2='';

    this.storage.get('user_data').then((val) => {
      this.userdata.push(val);
    });
  }

  ubah(){
    
  	if(
      this.dataForm.old_password=='' ||
      this.dataForm.new_password=='' ||
      this.dataForm.new_password2==''
    ){
      if(this.dataForm.old_password==''){
        this.err.old_password = "Harus Diisi!";
      }else{
        this.err.old_password = "";
      }

      if(this.dataForm.new_password==''){
        this.err.new_password = "Harus Diisi!";
      }else{
        this.err.new_password = "";
      }

      if(this.dataForm.new_password2==''){
        this.err.new_password2 = "Harus Diisi!";
      }else{
        this.err.new_password2 = "";
      }
    }else{

      if(this.dataForm.new_password2!=this.dataForm.new_password){
        this.err.new_password2 = "Password tidak sama!";
        return false;
      }else{
        this.err.new_password2 = ""; 
      }

      this.err.old_password = ""; 
      this.err.new_password = ""; 

      let confirm = this.alertCtrl.create({
        title: 'Ganti Password',
        subTitle: 'Apakah yakin mau merubah password lama anda?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
          },
          {
            text: 'Ubah Sekarang',
            handler: () => {
              this.loading = this.loadingCtrl.create({
                  content: 'Memproses...',
              });
              this.loading.present();

              this.ubah_process();
            }
          }
        ]
      });
      confirm.present();
    }
  }

  ubah_process(){
    let data = {
      email : this.userdata[0]['email'],
      old : this.dataForm.old_password,
      new : this.dataForm.new_password,
    }

    this.service.ubahSandi(data)
    .then(result => {
      if(result['resp']==false){
        this.err.old_password = "Password lama salah!";
        this.loading.dismiss();
      }else{
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
            title: 'Ganti Password Berhasil',
            subTitle: 'Password anda berhasil diperbaharui',
            buttons: ['OK']
        });

        alert.present();

        this.navCtrl.pop();
      }
    });
  }
}

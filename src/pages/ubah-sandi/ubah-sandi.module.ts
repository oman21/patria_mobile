import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UbahSandiPage } from './ubah-sandi';

@NgModule({
  declarations: [
    UbahSandiPage,
  ],
  imports: [
    IonicPageModule.forChild(UbahSandiPage),
  ],
})
export class UbahSandiPageModule {}

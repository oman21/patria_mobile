import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KatalogPage } from './katalog';

@NgModule({
  declarations: [
    KatalogPage,
  ],
  imports: [
    IonicPageModule.forChild(KatalogPage),
  ],
})
export class KatalogPageModule {}

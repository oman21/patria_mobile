import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';

/**
 * Generated class for the KatalogPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-katalog',
  templateUrl: 'katalog.html',
})
export class KatalogPage {

	produk:any = [];
	urlMaterial:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public service:ServiceProvider) {
  	this.getProduk();

  	this.service.urlMaterial().then(url=>{
  		this.urlMaterial = url;
  	});
  }

  getProduk(){
  	this.service.getAllProduk().then(res=>{
  		for(let i=0;i<res['data'].length;i++){
  			this.produk.push(res['data'][i]);
  		}
  	});
  }

  purchase(id, katalog_name){
  	this.navCtrl.push("PurchasePage", {id:id, katalog_name: katalog_name});
  }

}

import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-reward-patria',
  templateUrl: 'reward-patria.html',
})
export class RewardPatriaPage {
	items:any = [];
  itemsfilter:any = [];
  itemsall:any = [];
	collect:any = [];
	modal:any;
	tabs:any;
  loader:any;
  searchKey:any;
  filter:any = [];

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public storage:Storage,
    public service: ServiceProvider,
    public detectorRef: ChangeDetectorRef,
    public events: Events,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController
  ){

  }

  ionViewWillEnter() {

    this.filter[1] = true;
    this.filter[2] = true;
    this.filter[3] = true;
    this.filter[4] = true;

    this.loader = this.loadingCtrl.create({
        content: 'Memproses...',
    });
    this.loader.present();

    this.tabs = 'claim';
    this.storage.get('user_data').then((val)  => {
      let query = '';
      if(val.group.id==4){
        query = "t_order.status_order='finish' AND users.parent_id='"+val.user_id+"' AND t_order.is_claim_reward > '0'";
      }else{
        query = "t_order.status_order='finish' AND t_order.is_claim_reward > '0'";
      }
      
      this.service.getAllOrder(query).then(res=>{
        if(res['status']=='500'){
          this.loader.dismiss();
        }else{
          this.items = res['data']['data'];
          this.itemsfilter = res['data']['data'];
          for(let i=0;i<res['data']['data'].length;i++){
            this.itemsall.push(res['data']['data'][i]);
          }
          this.loader.dismiss();
        }
      });
    });

    this.modal = 'option-claim-close';
  }
 
 	rewardDetail(order_id){
  	this.navCtrl.push("RewardDetailPage", {order_id:order_id});
  }

  sorting() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Filter');

    alert.addInput({
      type: 'checkbox',
      label: 'Waiting',
      value: '2',
      checked: this.filter[2]
    });

    alert.addInput({
      type: 'checkbox',
      label: 'On Process',
      value: '3',
      checked: this.filter[3]
    });

    /*alert.addInput({
      type: 'checkbox',
      label: 'Rejected',
      value: '4',
      checked: this.filter.rejected
    });*/

    alert.addInput({
      type: 'checkbox',
      label: 'Verified Release',
      value: '4',
      checked: this.filter[4]
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Finish',
      value: '1',
      checked: this.filter[1]
    });

    alert.addButton('Batal');
    alert.addButton({
      text: 'Filter',
      handler: data => {
        this.items = [];
          
        this.filter[1] = false;
        this.filter[2] = false;
        this.filter[3] = false;
        this.filter[4] = false;

          for(let i=0;i<data.length;i++){
            this.filter[data[i]] = true;
            var newdata = this.itemsall.filter( function(item){
              return (item.is_claim_reward==data[i]);
            });

            if(newdata.length>0){
              for(let j=0;j<newdata.length;j++){
                this.items.push(newdata[j]);
              }
            }
          }

          console.log('myclaim',this.items);
        }
      });
    alert.present();
  }

  search_reward(nameKey, myArray){
    let new_data = [];
    for (var i=0; i < myArray.length; i++) {
      if(
        myArray[i]['nama_produk'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1 ||
        myArray[i]['full_name'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1
        
      ) {
        new_data.push(myArray[i]);
      }
    }

    this.items = new_data;
  }
}

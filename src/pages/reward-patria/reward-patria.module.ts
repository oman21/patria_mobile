import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RewardPatriaPage } from './reward-patria';

@NgModule({
  declarations: [
    RewardPatriaPage,
  ],
  imports: [
    IonicPageModule.forChild(RewardPatriaPage),
  ],
})
export class RewardPatriaPageModule {}

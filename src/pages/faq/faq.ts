import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {

	shownGroup = null;
	data:any = [];
  myInput:any;
  dataAll:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public service:ServiceProvider) {
    this.myInput = "";

  	this.service.getFaq().then(res=>{
  		for(let i=0;i<res['data'].length;i++){
  			this.data.push(res['data'][i]);
        this.dataAll.push(res['data'][i]);
  		}
  	});
  }

  onInput(e){
    console.log('myInput', this.myInput);

    var newList = this.dataAll.filter((item) => {
        return item.title.toLowerCase().indexOf(this.myInput.toLowerCase()) > -1;
    });

    this.data = newList;
  }

  onCancel(e){
    this.data = this.dataAll;
  }

  open(data){
    this.navCtrl.push("FaqDetailPage", {data:data});
  }

}

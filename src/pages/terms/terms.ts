import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { DomSanitizer } from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-terms',
  templateUrl: 'terms.html',
})
export class TermsPage {

	terms:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: ServiceProvider,protected sanitizer: DomSanitizer) {
  	this.service.terms().then(res=>{
  		console.log(res['data']['data']['terms_condition']);
  		this.terms = this.sanitizer.bypassSecurityTrustHtml(res['data']['data']['terms_condition']);
  	});
  }

}

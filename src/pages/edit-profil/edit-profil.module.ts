import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditProfilPage } from './edit-profil';
import { SignaturePadModule } from 'angular2-signaturepad';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [
    EditProfilPage,
  ],
  imports: [
    IonicPageModule.forChild(EditProfilPage),
    SignaturePadModule,
    IonicImageViewerModule,
  ],
})
export class EditProfilPageModule {}

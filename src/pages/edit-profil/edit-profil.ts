import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ActionSheetController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { DatabaseProvider } from '../../providers/database/database';

import { HomePage } from '../home/home';
import { Network } from '@ionic-native/network';

import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
import {SignaturePad} from 'angular2-signaturepad/signature-pad';

@IonicPage()
@Component({
  selector: 'page-edit-profil',
  templateUrl: 'edit-profil.html',
})
export class EditProfilPage {
  dataForm:any = [];
  data_error:any = [];
  loading:any;
  userdata:any = [];
  groupid:any;
	urlMaterial:any=[];
  signatureTemp:any = true;
  signatureNew:any = null;
  loading_dokumen:any = [];
  dokumenUrl:any = [];

  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  private signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 2,
    'canvasHeight': 200
  };

  constructor(
  	public navCtrl: NavController, 
    public navParams: NavParams,
    public network:Network,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public service: ServiceProvider,
    public database: DatabaseProvider,
    public storage:Storage,  
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController
  ) {
  	
  	this.service.urlMaterial().then(url=>{
  		this.urlMaterial.push(url);
  	});

  	this.storage.get('user_data').then((val) => {
  		this.groupid = val.group.id;
      this.dataForm = val;

      this.loading_dokumen['ktp'] = false;
      this.dokumenUrl['ktp'] = val.ktp?this.urlMaterial[0]+'foto_user/berkas/'+val.ktp:null;

      this.loading_dokumen['npwp'] = false;
      this.dokumenUrl['npwp'] = val.npwp?this.urlMaterial[0]+'foto_user/berkas/'+val.npwp:null;

      this.loading_dokumen['tabungan'] = false;
      this.dokumenUrl['tabungan'] = val.tabungan?this.urlMaterial[0]+'foto_user/berkas/'+val.tabungan:null;
      
      if(val.agreement_snk==1){
        this.dataForm.agreement_snk = true;
      }else{
        this.dataForm.agreement_snk = false;
      }

      if(val.agreement_skb==1){
        this.dataForm.agreement_skb = true;
      }else{
        this.dataForm.agreement_skb = false;
      }

      this.userdata.push(val);

      console.log(val);
    });
  }

  ngAfterViewInit() {
    if(this.signatureTemp==false){
      let canvas = document.querySelector('canvas');
      this.signaturePad.set('minWidth', 1);
      this.signaturePad.set('canvasWidth', canvas.offsetWidth);
      this.signaturePad.set('canvasHeight', canvas.offsetHeight);
      this.signaturePad.clear();
    }
  }

  drawComplete() {
    console.log("signature", this.signaturePad.toDataURL());
    this.signatureNew = this.signaturePad.toDataURL();
  }

  drawClear() {
    this.signaturePad.clear();
    this.signatureNew=null;
  }

  terms(){
    this.navCtrl.push("TermsPage");
  }

  skb_open(data){
    if(
      data.full_name=="" ||
      ((this.signatureTemp==true && this.dataForm.signature==null) &&  this.userdata[0]['group']['id']==5) ||
      ((this.signatureTemp==false && this.signatureNew==null) &&  this.userdata[0]['group']['id']==5) ||
      (data.nik=="" || data.nik==null) ||
      ((data.bank=="" || data.bank==null) && this.userdata[0]['group']['id']==5)||
      ((data.no_rekening=="" || data.no_rekening==null) && this.userdata[0]['group']['id']==5) ||
      ((data.rekening_atas_nama==""  || data.rekening_atas_nama==null) && this.userdata[0]['group']['id']==5) ||
      ((data.alamat_dealer=="" || data.alamat_dealer==null) && this.userdata[0]['group']['id']==5) ||
      data.agreement_snk == 0
    ){
      if(data.full_name==""){
        this.data_error.nama = "Nama harus diisi!";
      }else{
        this.data_error.nama = "";
      }

      if(data.nik==""||data.nik==null){
        this.data_error.nik = "NIK harus diisi!";
      }else{
        this.data_error.nik = "";
      }

      if(data.bank=="" || data.bank==null){
        this.data_error.bank = "Nama Bank harus diisi!";
      }else{
        this.data_error.bank = "";
      }

      if(data.no_rekening=="" || data.no_rekening==null){
        this.data_error.rek = "No.Rekening harus diisi!";
      }else{
        this.data_error.rek = "";
      }

      if(data.rekening_atas_nama=="" || data.rekening_atas_nama==null){
        this.data_error.rek_nama = "Rekening Atas Nama harus diisi!";
      }else{
        this.data_error.rek_nama = "";
      }

      if(data.alamat_dealer=="" || data.alamat_dealer==null){
        this.data_error.alamat_dealer = "Alamat Dealer harus diisi!";
      }else{
        this.data_error.alamat_dealer = "";
      }

      if(this.signatureTemp==true && this.dataForm.signature==null){
        this.data_error.signature_tmp = "Anda harus menandatangani ini";  
      }else{
        this.data_error.signature_tmp = ""; 
      }

      if(this.signatureTemp==false && this.dataForm.signatureNew==null){
        this.data_error.signature = "Anda harus menandatangani ini";  
      }else{
        this.data_error.signature = ""; 
      }

      if(data.agreement_snk==0){
        this.data_error.snk = "Anda harus menyetujui ini";
      }else{
        this.data_error.snk = "";
      }

      let alert = this.alertCtrl.create({
        title: 'Surat Keputusan Bersama',
        subTitle: 'Pastikan data anda sudah lengkap',
        buttons: ['OK']
      });
      alert.present();
    }else{
      if(this.signatureTemp==true){
        const toDataURL = url => fetch(url)
        .then(response => response.blob())
        .then(blob => new Promise((resolve, reject) => {
          const reader = new FileReader()
          reader.onloadend = () => resolve(reader.result)
          reader.onerror = reject
          reader.readAsDataURL(blob)
        }))


        toDataURL(this.urlMaterial[0]+'foto_user/berkas/'+data.signature)
        .then(dataUrl => {
          console.log('RESULT:', dataUrl);
          var new_data = {
            signature: dataUrl,
            nama : data.full_name,
            rek : data.no_rekening,
            rek_nama : data.rekening_atas_nama,
            nik : data.nik,
            alamat_dealer : data.alamat_dealer,
            bank : data.bank
          }
          this.navCtrl.push("SuratKeputusanBersamaPage", {data:new_data});
        })
      }else{
        var new_data = {
          signature: this.signatureNew,
          nama : data.full_name,
          rek : data.no_rekening,
          rek_nama : data.rekening_atas_nama,
          nik : data.nik,
          alamat_dealer : data.alamat_dealer,
          bank : data.bank
        }
        this.navCtrl.push("SuratKeputusanBersamaPage", {data:new_data});
      }
    }
  }

  simpan(){

  	if(this.network.type=="none"){
      let alert = this.alertCtrl.create({
        title: 'Koneksi Gagal!',
        subTitle: 'Pastikan perangkat anda terhubung dengan jaringan internet',
        buttons: ['OK']
      });
      alert.present();
    }else{
    	this.loading = this.loadingCtrl.create({
          content: 'Memproses...',
      });
      this.loading.present();
	  	if(
	  		this.dataForm.full_name=="" || 
	  		this.dataForm.email=="" ||
        ((this.dataForm.nik==""||this.dataForm.nik==null)&&this.userdata[0]['group']['id']==5) ||
		  	(this.dataForm.phone=="" || this.dataForm.phone==null) ||
		  	((this.dataForm.bank=="" || this.dataForm.bank==null) && this.userdata[0]['group']['id']==5)||
		  	((this.dataForm.no_rekening=="" || this.dataForm.no_rekening==null) && this.userdata[0]['group']['id']==5) ||
		  	((this.dataForm.rekening_atas_nama=="" || this.dataForm.rekening_atas_nama==null) && this.userdata[0]['group']['id']==5) ||
		  	((this.dataForm.dealer=="" || this.dataForm.dealer==null) && this.userdata[0]['group']['id']==5) ||
		  	((this.dataForm.alamat_dealer=="" || this.dataForm.alamat_dealer==null) && this.userdata[0]['group']['id']==5) ||
		  	((this.dokumenUrl['ktp']=="" || this.dokumenUrl['ktp']==null) && this.userdata[0]['group']['id']==5) ||
        ((this.dataForm.no_npwp=="" || this.dataForm.no_npwp==null) && this.userdata[0]['group']['id']==5) ||
		  	((this.dokumenUrl['npwp']=="" || this.dokumenUrl['npwp']==null) && this.userdata[0]['group']['id']==5) ||
		  	((this.dokumenUrl['tabungan']=="" || this.dokumenUrl['tabungan']==null ) && this.userdata[0]['group']['id']==5) ||
        (this.signatureTemp==true && this.dataForm.signature==null) ||
        (this.signatureTemp==false && this.signatureNew==null) ||
        (this.dataForm.agreement_snk==0 &&  this.userdata[0]['group']['id']==5) ||
        (this.dataForm.agreement_skb==0 &&  this.userdata[0]['group']['id']==5) ||
        (this.dataForm.lokasi==null &&  this.userdata[0]['group']['id']==4)
	  	){
	  		if(this.dataForm.full_name==""){
	  			this.data_error.nama = "Nama harus diisi!";
	  		}else{
	  			this.data_error.nama = "";
	  		}

        if(this.dataForm.no_npwp=="" || this.dataForm.no_npwp==null){
          this.data_error.no_npwp = "NPWP harus diisi!";
        }else{
          this.data_error.no_npwp = "";
        }

	  		if(this.dataForm.email==""){
	  			this.data_error.email = "Email harus diisi!";
	  		}else{
	  			this.data_error.email = "";
	  		}

        if(this.dataForm.nik=="" || this.dataForm.nik==null){
          this.data_error.nik = "NIK harus diisi!";
        }else{
          this.data_error.nik = "";
        }

	  		if(this.dataForm.phone=="" || this.dataForm.phone==null){
	  			this.data_error.hp = "No.Handphone harus diisi!";
	  		}else{
	  			this.data_error.hp = "";
	  		}

	  		if(this.dataForm.bank=="" || this.dataForm.bank==null){
	  			this.data_error.bank = "Nama Bank harus diisi!";
	  		}else{
	  			this.data_error.bank = "";
	  		}

	  		if(this.dataForm.no_rekening=="" || this.dataForm.no_rekening==null){
	  			this.data_error.rek = "No.Rekening harus diisi!";
	  		}else{
	  			this.data_error.rek = "";
	  		}

	  		if(this.dataForm.rekening_atas_nama=="" || this.dataForm.rekening_atas_nama==null){
	  			this.data_error.rek_nama = "Rekening Atas Nama harus diisi!";
	  		}else{
	  			this.data_error.rek_nama = "";
	  		}

	  		if(this.dataForm.dealer=="" || this.dataForm.dealer==null){
	  			this.data_error.dealer = "Nama Dealer harus diisi!";
	  		}else{
	  			this.data_error.dealer = "";
	  		}

	  		if(this.dataForm.alamat_dealer=="" || this.dataForm.alamat_dealer==null){
	  			this.data_error.alamat_dealer = "Alamat Dealer harus diisi!";
	  		}else{
	  			this.data_error.alamat_dealer = "";
	  		}

	  		if(this.dokumenUrl['ktp']==""||this.dokumenUrl['ktp']==null){
	  			this.data_error.ktp = "Photo ktp harus diupload!";
	  		}else{
	  			this.data_error.ktp = "";
	  		}

	  		if(this.dokumenUrl['npwp']==""||this.dokumenUrl['npwp']==null){
	  			this.data_error.npwp = "Photo npwp harus diupload!";
	  		}else{
	  			this.data_error.npwp = "";
	  		}

	  		if(this.dokumenUrl['tabungan']=="" || this.dokumenUrl['tabungan']==null){
	  			this.data_error.tabungan = "Photo tabungan harus diupload!";
	  		}else{
	  			this.data_error.tabungan = "";
	  		}

        if(this.signatureTemp==true && this.dataForm.signature==null){
          this.data_error.signature_tmp = "Anda harus menandatangani ini";  
        }else{
          this.data_error.signature_tmp = ""; 
        }

        if(this.signatureTemp==false && this.signatureNew==null){
          this.data_error.signature = "Anda harus menandatangani ini";  
        }else{
          this.data_error.signature = ""; 
        }

        if(this.dataForm.agreement_snk==0){
          this.data_error.snk = "Anda harus menyetujui ini";
        }else{
          this.data_error.snk = "";
        }

        if(this.dataForm.agreement_skb==0){
          this.data_error.skb = "Anda harus menyetujui ini";
        }else{
          this.data_error.skb = "";
        }

        if((this.dataForm.lokasi=="" || this.dataForm.lokasi==null) &&  this.userdata[0]['group']['id']==4){
          this.data_error.lokasi = "Lokasi harus diisi!";
        }else{
          this.data_error.lokasi = "";
        }

        let confirm = this.alertCtrl.create({
	        title: '',
	        subTitle: 'Masih ada data yang belum lengkap',
	        buttons: [
		        {
		          text: 'Ok',
		          role: 'ok',
		        }
		      ]
	      });
	      confirm.present();
	  		this.loading.dismiss();
	  	}else{
	  		this.loading.dismiss();

  			let confirm = this.alertCtrl.create({
	        title: 'Ubah Profil',
	        subTitle: 'Apakah anda yakin mau merubah data profil?',
	        buttons: [
		        {
		          text: 'Cancel',
		          role: 'cancel',
		        },
		        {
		          text: 'Ubah Sekarang',
		          handler: () => {
		            this.loading = this.loadingCtrl.create({
		                content: 'Memproses...',
		            });
		            this.loading.present();

		            this.edit_process();
		          }
		        }
		      ]
	      });
	      confirm.present();
  		}
  	}
  }

  edit_process(){
  	let data = {
  		email : this.dataForm.email,
      full_name : this.dataForm.full_name,
      phone : this.dataForm.phone,
      bank : this.dataForm.bank,
      nik : this.dataForm.nik,
      no_npwp : this.dataForm.no_npwp,
      no_rekening : this.dataForm.no_rekening,
      rekening_atas_nama : this.dataForm.rekening_atas_nama,
      dealer : this.dataForm.dealer,
      alamat_dealer : this.dataForm.alamat_dealer,
      signature: this.signatureNew!=null?this.signatureNew.replace('data:image/png;base64,',''):null,
      lokasi : this.dataForm.lokasi,
  	}

  	this.service.editProfil(this.userdata[0]['user_id'], data)
    .then(result => {
    	if(result['status']==200){

    		this.loading.dismiss();

        var signatureNew;
        if(result['signature']!=""){
          signatureNew = result['signature'];
        }else{
          signatureNew = this.userdata[0]['signature'];
        }       
          

    		let new_userdata = {
    			activation_code: this.userdata[0]['activation_code'],
					active:this.userdata[0]['active'],
					alamat_dealer:this.dataForm.alamat_dealer,
					bank:this.dataForm.bank,
					created_on:this.userdata[0]['created_on'],
					dealer:this.dataForm.dealer,
					email:this.dataForm.email,
          nik:this.dataForm.nik,
          no_npwp:this.dataForm.no_npwp,
					first_name:this.userdata[0]['first_name'],
					forgotten_password_code:this.userdata[0]['forgotten_password_code'],
					forgotten_password_time:this.userdata[0]['forgotten_password_time'],
					full_name:this.dataForm.full_name,
					group:{
						description:this.userdata[0]['group']['description'],
						id:this.userdata[0]['group']['id'],
						name:this.userdata[0]['group']['name']
					},
					id:this.userdata[0]['id'],
					ip_address:this.userdata[0]['ip_address'],
					last_login:this.userdata[0]['last_login'],
					last_name:this.userdata[0]['last_name'],
					no_rekening:this.dataForm.no_rekening,
					rekening_atas_nama:this.dataForm.rekening_atas_nama,
					parent_id:this.userdata[0]['parent_id'],
					password:this.userdata[0]['password'],
					phone:this.dataForm.phone,
					photo:this.userdata[0]['photo'],
					remember_code:this.userdata[0]['remember_code'],
					salt:this.userdata[0]['salt'],
					user_id:this.userdata[0]['user_id'],
					username:this.userdata[0]['username'],
          ktp: this.userdata[0]['ktp'],
          npwp: this.userdata[0]['npwp'],
          tabungan: this.userdata[0]['tabungan'],
          signature: signatureNew,
          surat_kesepakatan: result['surat_kesepakatan']==""?this.userdata[0]['surat_kesepakatan']:result['surat_kesepakatan'],
          agreement_snk: this.dataForm.agreement_snk,
          agreement_skb: this.dataForm.agreement_skb,
          lokasi:this.dataForm.lokasi,
    		}

        console.log("new_userdata", new_userdata);

    		this.database.insertUser(new_userdata);

    		let alert = this.alertCtrl.create({
		        title: 'Edit Profil Berhasil',
		        subTitle: 'Profil anda berhasil diperbaharui',
		        buttons: ['OK']
		    });

		    alert.present();

		    this.data_error.tabungan = "";
  			this.data_error.npwp = "";
  			this.data_error.ktp = "";
  			this.data_error.alamat_dealer = "";
  			this.data_error.dealer = "";
  			this.data_error.rek_nama = "";
  			this.data_error.rek = "";
  			this.data_error.bank = "";
  			this.data_error.hp = "";
  			this.data_error.email = "";
  			this.data_error.nama = "";
    	}
    });
  }

  ganti_foto(type){
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Pilih Mode',
      buttons: [
        {
          text: 'Kamera',
          handler: () => {
            if(type=="ktp"){
              this.uploadKTP();
            }else if(type=="npwp"){
              this.uploadNPWP();
            }else if(type=="tabungan"){
              this.uploadTABUNGAN();
            }
          }
        },{
          text: 'Gallery',
          handler: () => {
            if(type=="ktp"){
              this.galleryKTP();
            }else if(type=="npwp"){
              this.galleryNPWP();
            }else if(type=="tabungan"){
              this.galleryTABUNGAN();
            }
          }
        },{
          text: 'Batal',
          role: 'cancel',
        }
      ]
    });
    actionSheet.present();
  }

  uploadKTP(){
  	const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: true
    }

    this.camera.getPicture(options).then((imageData) => {
    	this.uploadDokumen('ktp', imageData);
    }, (err) => {
      console.log(err);
    });
  }

  uploadNPWP(){
  	const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: true
    }

    this.camera.getPicture(options).then((imageData) => {
    	this.uploadDokumen('npwp', imageData);
    }, (err) => {
      console.log(err);
    });
  }

  uploadTABUNGAN(){
  	const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: true
    }

    this.camera.getPicture(options).then((imageData) => {
    	this.uploadDokumen('tabungan', imageData);
    }, (err) => {
      console.log(err);
    });
  }

  uploadDokumen(type, file){
    this.loading_dokumen[type] = true;

    this.service.UploadDokumen(type, file, this.userdata[0]['user_id'], this.dataForm.full_name).then(res=>{
      if(res['status']=='200'){
        this.loading_dokumen[type] = false;
        this.dokumenUrl[type] = this.urlMaterial[0]+'foto_user/berkas/'+res['file'];
        this.userdata[0][type] = res['file'];
        this.database.insertUser(this.userdata[0]);
      }else{
        alert('Upload Gagal');
        this.loading_dokumen[type] = false;
      }
    })
  }

  galleryKTP(){
    const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: false
    }

    this.camera.getPicture(options).then((imageData) => {
      this.uploadDokumen('ktp', imageData);
    }, (err) => {
      console.log(err);
    });
  }

  galleryNPWP(){
    const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: false
    }

    this.camera.getPicture(options).then((imageData) => {
      this.uploadDokumen('npwp', imageData);
    }, (err) => {
      console.log(err);
    });
  }

  galleryTABUNGAN(){
    const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: false
    }

    this.camera.getPicture(options).then((imageData) => {
      this.uploadDokumen('tabungan', imageData);
    }, (err) => {
      console.log(err);
    });
  }

  edit_signature(){
    this.signatureTemp = false;
  }

  cancel_signature(){
    this.signatureTemp = true;
    this.signatureNew=null;
  }

}

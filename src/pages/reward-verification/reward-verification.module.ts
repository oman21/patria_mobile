import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RewardVerificationPage } from './reward-verification';

@NgModule({
  declarations: [
    RewardVerificationPage,
  ],
  imports: [
    IonicPageModule.forChild(RewardVerificationPage),
  ],
})
export class RewardVerificationPageModule {}

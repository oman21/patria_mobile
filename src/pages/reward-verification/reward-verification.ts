import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ActionSheetController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { DatabaseProvider } from '../../providers/database/database';

import { Storage } from '@ionic/storage';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-reward-verification',
  templateUrl: 'reward-verification.html',
})
export class RewardVerificationPage {
  dataClaim:any = {};
  dataForm:any = [];
  data_error:any = [];
  loading:any;
  userdata:any = [];
  groupid:any;
  loader:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public service: ServiceProvider,
    public database: DatabaseProvider,
    public storage:Storage
    ) {
    
    let param_data = this.navParams.get('data');
    this.dataClaim = param_data.data_claim;
    this.dataForm.invoice = '';
    this.dataForm.kontrak = '';
    this.dataForm.note = '';
  }

  ionViewWillEnter() {
    this.userdata = [];
    this.storage.get('user_data').then((val) => {
      this.groupid = val.group.id;
      this.userdata.push(val);
      console.log(this.userdata);
    })
  }

  edit_profile(){
    this.navCtrl.push("EditProfilPage");
  }

  memo(){
    if(
      this.dataForm.invoice==""||
      this.dataForm.kontrak==""||
      this.dataForm.note==""||
      (this.userdata[0].lokasi==null||this.userdata[0].lokasi=='')||
      (this.userdata[0].signature==null||this.userdata[0].signature=='')
    ){
      if(this.dataForm.invoice==''){
        this.data_error.invoice="Harus diisi!";
      }else{
        this.data_error.invoice="";
      }

      if(this.dataForm.kontrak==''){
        this.data_error.kontrak="Harus diisi!";
      }else{
        this.data_error.kontrak="";
      }

      if(this.dataForm.note==''){
        this.data_error.note="Harus diisi!";
      }else{
        this.data_error.note="";
      }
    }else{
      this.data_error.invoice="";
      this.data_error.kontrak="";
      this.data_error.note="";

      console.log('this.dataClaim', this.dataClaim);
      let data_memo = {
        invoice: this.dataForm.invoice,
        kontrak: this.dataForm.kontrak,
        note: this.dataForm.note,
        user_id: this.userdata[0].user_id,
        signature: this.userdata[0].signature,
        order_id: this.dataClaim.id
      }

      this.navCtrl.push("InternalMemoPage", {data:data_memo});
    }
  }

  submit(){
    if(
      this.dataForm.invoice==""||
      this.dataForm.kontrak==""||
      this.dataForm.note==""||
      (this.userdata[0].lokasi==null||this.userdata[0].lokasi=='')||
      (this.userdata[0].signature==null||this.userdata[0].signature=='')
    ){
      if(this.dataForm.invoice==''){
        this.data_error.invoice="Harus diisi!";
      }else{
        this.data_error.invoice="";
      }

      if(this.dataForm.kontrak==''){
        this.data_error.kontrak="Harus diisi!";
      }else{
        this.data_error.kontrak="";
      }

      if(this.dataForm.note==''){
        this.data_error.note="Harus diisi!";
      }else{
        this.data_error.note="";
      }
    }else{
      this.data_error.invoice="";
      this.data_error.kontrak="";
      this.data_error.note="";

      let alert = this.alertCtrl.create();
      alert.setTitle('Apakah anda yakin mau verified claim ini dan mengajukan approval ke manager?');

      alert.addButton('Batal');
      alert.addButton({
        text: 'Ya',
        handler: data => {
          this.verified_process();        
        }
      });
      alert.present();  
    }
  }

  verified_process(){
  	this.loader = this.loadingCtrl.create({
	      content: 'Memproses...',
	  });
	  this.loader.present();

  	let data = {
  			user_id: this.userdata[0]['user_id'],
        id_order: this.dataClaim.id,
        status: 'verified',
        keterangan: 'Reward di verified, menunggu approval manager',
        created_date: moment().format('YYYY-MM-DD h:mm:s')
		}

		this.service.insertClaim(data).then(res=>{console.log('insert claim', res);});

		let update = {
			id: this.dataClaim.id,
      is_claim_reward: 3,
      memo_invoice: this.dataForm.invoice,
      memo_kontrak: this.dataForm.kontrak,
      memo_note: this.dataForm.note,
      claim_verified_bc_id: this.userdata[0]['user_id'],
      claim_verified_bc_name: this.userdata[0]['full_name'],
		}

		this.service.confirmOrder(update)
    .then(res=>{
      this.loader.dismiss();

      //create pdf file
      // let data_memo = {
      //   full_name: this.userdata[0].full_name,
      //   signature: this.userdata[0].signature,
      //   invoice: this.dataForm.invoice,
      //   kontrak: this.dataForm.kontrak,
      //   note: this.dataForm.note,
      //   user_id: this.userdata[0].user_id,
      //   order_id: this.dataClaim.id
      // }
      // this.service.createMemoFile(data_memo);
      
      let dataInboxDealer = {
        id_relation: this.dataClaim.id,
        tipe_relation: "RewardDetailPage",
        user_id_from: this.userdata[0]['user_id'],
        user_id_to: this.dataClaim.user_id,
        judul: "Reward Sedang Diproses",
        content: "Claim reward anda dalam proses, sedang menunggu eskalasi ke Sales Manager",
      }

      this.service.createInbox(dataInboxDealer).then(res_inbox=>{
        let pushDealer = {
          data:{
            message: "Reward Sedang Diproses",
              tipe: "claim_reward",
              page: "InboxDetilPage",
              parram: {
                inbox_id:res_inbox['data']['id']
              }
          }
        }

        this.service.pushNotifikasi(this.dataClaim.user_id, pushDealer);
      });

      this.service.getManager().then(resMgr=>{
        
        console.log("masuk ke claim reward diverifikasi untuk manager");

        for(var i=0;i<resMgr['data'].length;i++){

          var idManager = resMgr['data'][i]['user_id'];

          let dataInboxMgr = {
            id_relation: this.dataClaim.id,
            tipe_relation: "RewardDetailPage",
            user_id_from: this.userdata[0]['user_id'],
            user_id_to: idManager,
            judul: "Internal Memo Approval Request",
            content: "Business Consultant membuat internal memo baru, silahkan melakukan approval",
          }

          this.sendpushmgrClaim(idManager, dataInboxMgr);
        }
      });

      let alert = this.alertCtrl.create({
        title: 'Telah Diverfied',
        subTitle: 'Silahkan follow up manager untuk approval',
        buttons: [
          {
            text: 'Tutup',
          }
        ]
      });
      alert.present();

      this.navCtrl.pop();
    });
  }
  
  sendpushmgrClaim(idManager, dataInboxMgr){
    console.log("manager claim reward", idManager, dataInboxMgr);
    this.service.createInbox(dataInboxMgr).then(res_inbox=>{
      let pushMgr = {
        data:{
          message: "Claim Reward diverifikasi",
            tipe: "order_confirm",
            page: "InboxDetilPage",
            parram: {
              inbox_id:res_inbox['data']['id']
            }
        }
      }

      this.service.pushNotifikasi(idManager, pushMgr);
    });
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
import { DomSanitizer } from '@angular/platform-browser';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
/**
 * Generated class for the SalesDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sales-detail',
  templateUrl: 'sales-detail.html',
})
export class SalesDetailPage {
	tabsprofil:any;
	salesDetail:any = [];
	urlMaterial:any;
	salespatria:any = [];
	salesdealer:any = [];
  groupid:any;
  loader:any;
  salesDetailGroupId:any;
  userdata:any = [];

  constructor(protected sanitizer: DomSanitizer, public navCtrl: NavController, public navParams: NavParams, public service:ServiceProvider, public alertCtrl: AlertController,  public storage:Storage, public loadingCtrl: LoadingController,private inAppBrowser:InAppBrowser) {
    this.loader = this.loadingCtrl.create({
        content: 'Memproses...',
    });
    this.loader.present();

  	this.tabsprofil = 'profil';
  	this.service.urlMaterial().then(url=>{
  		this.urlMaterial = url;
  	});
  	this.getSales(this.navParams.get('user_id'));
  	this.service.getAllSales(4).then(res=>{
  		for(let i=0;i<res['data'].length;i++){
  			this.salespatria.push(res['data'][i]);
        this.loader.dismiss();
  		}
  	});

    this.storage.get('user_data').then((val) => {
      this.groupid = val.group.id;
    });

  	this.getSalesDealer();
  }

  ionViewWillEnter() {
    this.userdata = [];
    this.storage.get('user_data').then((val) => {
      this.userdata.push(val);
    })
  }

  getSales(id){
  	this.service.getSalesById(id).then(res=>{
  		console.log(res['data']);
  		this.salesDetail = res['data'];
      this.salesDetailGroupId = res['data']['group']['id'];
  	});
  }

  getSalesDealer(){
  	this.service.getAllSalesDealer(this.navParams.get('user_id')).then(res=>{
  		for(let i=0;i<res['data'].length;i++){
  			this.salesdealer.push(res['data'][i]);
  		}
  	});
  }

  verifikasi() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Pilih Sales Patria');

    for(let i=0;i<this.salespatria.length;i++){
	    alert.addInput({
	      type: 'radio',
	      label: this.salespatria[i].full_name,
	      value: this.salespatria[i].user_id,
	      checked: false
	    });
	  }

    alert.addButton('Batal');
    alert.addButton({
      text: 'Verifikasi',
      handler: data => {
        if(typeof data!='undefined' || data!=''){
        	this.service.konfirmasiUser(this.navParams.get('user_id'), data, 1).then(res=>{
            this.salesDetail.active=1;
            
            let dataInboxSales = {
              id_relation: this.navParams.get('user_id'),
              tipe_relation: "ProfilePage",
              user_id_from: this.userdata[0]['user_id'],
              user_id_to: this.navParams.get('user_id'),
              judul: "Akun Diverifikasi",
              content: "Akun anda telah di verifikasi silahkan login",
            }

            this.service.createInbox(dataInboxSales).then(()=>{

              let pushSalesDealer = {
                data: {
                  message: "Akun anda telah di verifikasi silahkan login",
                  tipe: "verifikasi_berhasil",
                  page: "LoginPage",
                }
              }

              this.service.pushNotifikasi(this.navParams.get('user_id'), pushSalesDealer);
            })

            let dataInboxSalesPatria = {
              id_relation: this.navParams.get('user_id'),
              tipe_relation: "SalesDetailPage",
              user_id_from: this.userdata[0]['user_id'],
              user_id_to: data,
              judul: "Sales Baru",
              content: "Anda telah menerima sales baru",
            }

            this.service.createInbox(dataInboxSalesPatria).then(()=>{
              let pushSalesPatria = {
                data: {
                  message: "Anda telah menerima sales baru",
                  tipe: "verifikasi_berhasil",
                  page: "SalesDetailPage",
                  parram: {
                    user_id:this.navParams.get('user_id')
                  }
                }
              }

              this.service.pushNotifikasi(data, pushSalesPatria);
            })

            this.service.notifSalesBaru();
        	});
        }
      }
    });
    alert.present();
  }

  tolak(){
  	let alert = this.alertCtrl.create();
    alert.setTitle('Apakah anda yakin mau menolak sales ini?');

    alert.addButton('Batal');
    alert.addButton({
      text: 'Ya',
      handler: data => {
        this.service.konfirmasiUser(this.navParams.get('user_id'), data, 2).then(res=>{
      		this.salesDetail.active=2;

          let pushSalesDealer = {
            data: {
              message: "Mohon maaf pendaftaran sales patria anda di tolak",
              tipe: "verifikasi_gagal",
              page: "AuthStatePage",
            }
          };
      		
          this.service.pushNotifikasi(this.navParams.get('user_id'), pushSalesDealer);

          this.service.notifSalesBaru();
      	});
      }
    });
    alert.present();	
  }

  detail(user_id){
  	this.navCtrl.push('SalesDetailPage', {user_id:user_id});
  }

  safe(value){
    return this.sanitizer.bypassSecurityTrustUrl(value);
  }

  skb(pdf){
    console.log("surat_kesepakatan",pdf);
    this.service.urlMaterial().then(url=>{
      var options:any = {
        location: 'no',
        clearcache: 'yes'
      };

      var timestamp = new Date().getTime();
      this.inAppBrowser.create(url+'surat_kesepakatan/'+pdf+'?'+timestamp, '_system', options);
    });
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesDetailPage } from './sales-detail';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [
    SalesDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesDetailPage),
    IonicImageViewerModule
  ],
})
export class SalesDetailPageModule {}

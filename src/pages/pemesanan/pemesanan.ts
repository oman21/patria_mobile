import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { Events } from 'ionic-angular';


/**
 * Generated class for the PemesananPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pemesanan',
  templateUrl: 'pemesanan.html',
})
export class PemesananPage {

	items:any = [];
  itemsfilter:any = [];
  itemsall:any = [];
  loader:any;
  groupid:any;

  filter:any = [];
  searchKey:any;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public storage:Storage, 
  	public service: ServiceProvider,
    public events: Events,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController
  ) {
  	
  }

  ionViewWillEnter() {
    this.filter['waiting'] = true;
    this.filter['verified'] = true;
    this.filter['rejected'] = true;
    this.filter['po_process'] = true;
    this.filter['hold'] = true;
    this.filter['canceled'] = true;
    this.filter['on_process'] = true;
    this.filter['rejected_po'] = true;
    this.filter['delivered'] = true;
    this.filter['req_released_reward'] = true;
    this.filter['finish'] = true;

    this.storage.get('user_data').then((val) => {
      this.groupid = val.group.id;
    });

    this.loader = this.loadingCtrl.create({
        content: 'Memproses...',
    });
    this.loader.present();

    this.getOrder();

    this.events.subscribe('event_order', language => {
      this.getOrder();
    });
  }

  getOrder(){
    this.items = [];
  	this.storage.get('user_data').then((val) 	=> {
  		let query;
  		
  		if(val.group.id==3){
  			query = 't_order.user_id!="" order by t_order.id desc';	
  		}else if(val.group.id==4){
  			query = 'users.parent_id='+val.user_id+' order by t_order.id desc';	
  		}else if(val.group.id==5){
  			query = 't_order.user_id='+val.user_id+' order by t_order.id desc';	
  		}
  		
	  	this.service.getAllOrder(query).then(res=>{
        if(res['status']=='500'){
          this.loader.dismiss();
        }else{
          for(let i=0;i<res['data']['data'].length;i++){
            this.items.push(res['data']['data'][i]);
            this.itemsfilter.push(res['data']['data'][i]);
            this.itemsall.push(res['data']['data'][i]);
            this.loader.dismiss();
          }
        }
	  	});
	  });
  }

  sorting() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Filter');

    alert.addInput({
      type: 'checkbox',
      label: 'Waiting',
      value: 'waiting',
      checked: this.filter['waiting']
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Verified',
      value: 'verified',
      checked: this.filter['verified']
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Rejected',
      value: 'rejected',
      checked: this.filter['rejected']
    });

    alert.addInput({
      type: 'checkbox',
      label: 'PO Process',
      value: 'po_process',
      checked: this.filter['po_process']
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Hold',
      value: 'hold',
      checked: this.filter['hold']
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Canceled',
      value: 'canceled',
      checked: this.filter['canceled']
    });

    alert.addInput({
      type: 'checkbox',
      label: 'ON Process',
      value: 'on_process',
      checked: this.filter['on_process']
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Rejected PO',
      value: 'rejected_po',
      checked: this.filter['rejected_po']
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Delivered',
      value: 'delivered',
      checked: this.filter['delivered']
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Request Released Reward',
      value: 'req_released_reward',
      checked: this.filter['req_released_reward']
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Finish',
      value: 'finish',
      checked: this.filter['finish']
    });

    alert.addButton('Batal');
    alert.addButton({
      text: 'Filter',
      handler: data => {
        this.items = [];

        this.filter['waiting'] = false;
        this.filter['verified'] = false;
        this.filter['rejected'] = false;
        this.filter['po_process'] = false;
        this.filter['hold'] = false;
        this.filter['canceled'] = false;
        this.filter['on_process'] = false;
        this.filter['rejected_po'] = false;
        this.filter['delivered'] = false;
        this.filter['req_released_reward'] = false;
        this.filter['finish'] = false;

        for(let i=0;i<data.length;i++){
          this.filter[data[i]] = true;
          var newdata = this.itemsall.filter( function(item){
            return (item.status_order==data[i]);
          });

          if(newdata.length>0){
            for(let j=0;j<newdata.length;j++){
              this.items.push(newdata[j]);
            }
          }
        }
      }
    });
    alert.present();
  }

  detil(id){
  	this.navCtrl.push("PemesananDetilPage", {order_id:id});
  }

  neworder(){
    this.navCtrl.push("KatalogPage");
  }

  search_sales(nameKey, myArray){
    let new_data = [];
    for (var i=0; i < myArray.length; i++) {
      if(
        myArray[i]['nama_produk'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1 ||
        myArray[i]['nama_customer'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1 ||
        myArray[i]['nama_proyek'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1 ||
        myArray[i]['full_name'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1 ||
        myArray[i]['nama_parent'].toLowerCase().indexOf(nameKey.toLowerCase()) > -1
        
      ) {
        new_data.push(myArray[i]);
      }
    }

    this.items = new_data;
  }

}
